import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { AuthGuard } from '@app/shared/guards/auth.guard';
import { RoleEnum } from './shared/enums';
var routes = [
    {
        path: 'auth',
        canActivate: [AuthGuard], data: { role: [] },
        loadChildren: './pages/authorization.module#AuthorizationModule'
    },
    {
        path: '',
        canActivate: [AuthGuard], data: { role: [RoleEnum.admin, RoleEnum.user] },
        loadChildren: './tabs/tabs.module#TabsPageModule'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map