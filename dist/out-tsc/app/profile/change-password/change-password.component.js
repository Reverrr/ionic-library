import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@app/shared/services';
var ChangePasswordComponent = /** @class */ (function () {
    function ChangePasswordComponent(userService) {
        this.userService = userService;
        this.editPasswordForm = new FormGroup({
            currentPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]),
            newPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]),
        });
    }
    ChangePasswordComponent.prototype.savePassword = function () {
        var _this = this;
        this.userService.checkUserPassword(this.user, this.editPasswordForm.value).subscribe(function (data) {
            _this.passwordError = '';
            if (data.status === 201) {
                _this.user = data.data;
            }
        }, function (err) {
            _this.passwordError = err;
            throw err;
        });
    };
    ChangePasswordComponent = tslib_1.__decorate([
        Component({
            selector: 'app-change-password',
            templateUrl: 'change-password.component.html',
            styleUrls: ['./change-password.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [UserService])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());
export { ChangePasswordComponent };
//# sourceMappingURL=change-password.component.js.map