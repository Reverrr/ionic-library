import * as tslib_1 from "tslib";
//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile.component';
import { SharedModule } from '@app/shared/shared.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
var routes = [
    {
        path: '',
        component: ProfileComponent,
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    }
];
var ProfileModule = /** @class */ (function () {
    function ProfileModule() {
    }
    ProfileModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                ProfileComponent,
                ChangePasswordComponent
            ],
            imports: [
                IonicModule,
                CommonModule,
                SharedModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            exports: [
                ProfileComponent,
                ChangePasswordComponent
            ]
        })
    ], ProfileModule);
    return ProfileModule;
}());
export { ProfileModule };
//# sourceMappingURL=profile.module.js.map