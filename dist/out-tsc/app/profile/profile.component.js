import * as tslib_1 from "tslib";
//Vendors
import { Component } from '@angular/core';
import { UserModel } from '@app/shared/models';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserService, AuthenticationService } from '@app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from "@angular/common";
import { ModalController } from '@ionic/angular';
import { themeStylesConst } from '@environments/theme-styles';
import { themeStylesEnum } from '@app/shared/enums/theme-styles-enum';
import { MenuService } from '../shared/services/menu.service';
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(userService, route, router, location, auth, modalController, menuService) {
        var _this = this;
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.location = location;
        this.auth = auth;
        this.modalController = modalController;
        this.menuService = menuService;
        this.user = new UserModel();
        this.enum = themeStylesEnum;
        this.editFormGroup = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18), Validators.pattern("^[[A-Za-z0-9]+]*$")]),
            firstName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern("^[a-zA-Z]*$")]),
            lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern("^[a-zA-Z]*$")]),
            role: new FormControl('user', []),
        });
        this.auth.checkCurrentUser().subscribe(function (data) {
            _this.currentUser = data.userModel;
            // this.editFormGroup.setValue({
            //     username: data.userModel.username,
            //     firstName: data.userModel.firstName,
            //     lastName: data.userModel.lastName,
            //     role: data.userModel.role,
            // });
            _this.editFormGroup.setValue({
                username: '',
                firstName: '',
                lastName: '',
                role: '',
            });
            _this.user = data.userModel;
        });
    }
    ProfileComponent.prototype.ngOnInit = function () { };
    ProfileComponent.prototype.openModal = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                this.router.navigateByUrl('/tabs/profile/');
                return [2 /*return*/];
            });
        });
    };
    ProfileComponent.prototype.checkCurrentUser = function () {
        var _this = this;
        this.auth.checkCurrentUser().subscribe(function (data) {
            _this.currentUser = data.userModel;
        });
    };
    ProfileComponent.prototype.selectedTheme = function (event) {
        if (event.detail.value === this.enum.light) {
            this.userService.setColorTheme(themeStylesConst.light);
        }
        else if (event.detail.value === this.enum.dark) {
            this.userService.setColorTheme(themeStylesConst.dark);
        }
        else if (event.detail.value === this.enum.toxic) {
            this.userService.setColorTheme(themeStylesConst.toxic);
        }
        else if (event.detail.value === this.enum.normal) {
            this.userService.setColorTheme(themeStylesConst.normal);
        }
    };
    ProfileComponent.prototype.saveEditForm = function () {
        var _this = this;
        if (this.editFormGroup.valid) {
            for (var data in this.editFormGroup.value) {
                if (this.editFormGroup.value[data]) {
                    this.user[data] = this.editFormGroup.value[data];
                }
            }
            this.userService.update(this.user).pipe().subscribe(function (data) {
                _this.checkCurrentUser();
                _this.location.back();
            }, function (err) {
                _this.errorMessage = 'Username already exist';
            });
        }
    };
    ProfileComponent.prototype.backToPrevPage = function () {
        this.menuService.openMenuEvent();
        this.router.navigateByUrl('/tabs/library');
    };
    ProfileComponent = tslib_1.__decorate([
        Component({
            selector: 'app-profile',
            templateUrl: 'profile.component.html',
            styleUrls: ['profile.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [UserService,
            ActivatedRoute,
            Router,
            Location,
            AuthenticationService,
            ModalController,
            MenuService])
    ], ProfileComponent);
    return ProfileComponent;
}());
export { ProfileComponent };
//# sourceMappingURL=profile.component.js.map