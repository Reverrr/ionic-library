import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
var PropertyNameChangeHelper = /** @class */ (function () {
    function PropertyNameChangeHelper() {
    }
    PropertyNameChangeHelper.prototype.changeKeyName = function (model, newName, changeName) {
        var temp = Object.assign({}, model);
        temp["" + newName] = temp["" + changeName];
        delete temp["" + changeName];
        return temp;
    };
    PropertyNameChangeHelper.prototype.changeKeyNameFromArray = function (model, newName, changeName) {
        var _this = this;
        var temp = [];
        model.forEach(function (item) {
            temp.push(_this.changeKeyName(item, newName, changeName));
        });
        return temp.slice();
    };
    PropertyNameChangeHelper = tslib_1.__decorate([
        Injectable({ providedIn: 'root' })
    ], PropertyNameChangeHelper);
    return PropertyNameChangeHelper;
}());
export { PropertyNameChangeHelper };
//# sourceMappingURL=property-name-change.service.js.map