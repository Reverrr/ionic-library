import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
//Environment
import { environment } from '@environments/environment';
//Services
import { PropertyNameChangeHelper } from '@app/shared/services/property-name-change.service';
var LibraryService = /** @class */ (function () {
    function LibraryService(http, helper) {
        this.http = http;
        this.helper = helper;
        this.bookBS = new BehaviorSubject(this.book);
    }
    LibraryService.prototype.getBookById = function (id) {
        var _this = this;
        return this.http.get(environment.apiUrl + "/books/" + id)
            .pipe(map(function (book) {
            book = _this.helper.changeKeyName(book, 'id', '_id');
            return book;
        }));
    };
    LibraryService.prototype.update = function (book) {
        var _this = this;
        return this.http.put(environment.apiUrl + "/books/" + book.id, book)
            .pipe(map(function (book) {
            book = _this.helper.changeKeyName(book, 'id', '_id');
            return book;
        }));
    };
    LibraryService.prototype.create = function (book) {
        var _this = this;
        return this.http.post(environment.apiUrl + "/books/", book)
            .pipe(map(function (book) {
            book = _this.helper.changeKeyName(book, 'id', '_id');
            return book;
        }));
    };
    LibraryService.prototype.getAllAuthors = function () {
        var _this = this;
        return this.http.get(environment.apiUrl + "/authors")
            .pipe(map(function (authors) {
            authors = _this.helper.changeKeyNameFromArray(authors, 'id', '_id');
            return authors;
        }));
    };
    LibraryService.prototype.removeSelectedAuthor = function (id) {
        var _this = this;
        return this.http.delete(environment.apiUrl + "/authors/" + id)
            .pipe(map(function (author) {
            author = _this.helper.changeKeyName(author, 'id', '_id');
            return author;
        }));
    };
    LibraryService.prototype.deleteBook = function (id) {
        var _this = this;
        return this.http.delete(environment.apiUrl + "/books/" + id)
            .pipe(map(function (book) {
            book = _this.helper.changeKeyName(book, 'id', '_id');
            return book;
        }));
    };
    LibraryService.prototype.addNewAuthor = function (author) {
        return this.http.post(environment.apiUrl + "/authors", author);
    };
    LibraryService.prototype.getBooksForPagination = function (currentPage, count) {
        var _this = this;
        return this.http.get(environment.apiUrl + "/books/?page=" + currentPage + "&_limit=" + count)
            .pipe(map(function (book) {
            book = _this.helper.changeKeyNameFromArray(book, 'id', '_id');
            return book;
        }));
    };
    LibraryService.prototype.getTotalElements = function () {
        return this.http.get(environment.apiUrl + "/count/");
    };
    LibraryService.prototype.getSelectedBook = function () {
        return this.bookBS.asObservable();
    };
    LibraryService.prototype.searchBooks = function (request, count, authors, type, page) {
        var _this = this;
        if (request === void 0) { request = ''; }
        if (count === void 0) { count = 12; }
        if (authors === void 0) { authors = []; }
        if (type === void 0) { type = ''; }
        if (page === void 0) { page = 1; }
        return this.http.get(environment.apiUrl + "/books?q=" + request + "&_limit=" + count + "&authors=" + authors + "&type=" + type + "&page=" + page)
            .pipe(map(function (book) {
            book = _this.helper.changeKeyNameFromArray(book, 'id', '_id');
            return book;
        }));
    };
    LibraryService.prototype.dispatch = function (book) {
        this.next(book);
    };
    LibraryService.prototype.next = function (book) {
        this.book = book;
        this.bookBS.next(book);
    };
    LibraryService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            PropertyNameChangeHelper])
    ], LibraryService);
    return LibraryService;
}());
export { LibraryService };
//# sourceMappingURL=library.service.js.map