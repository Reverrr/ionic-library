import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
//Environment
import { environment } from '@environments/environment';
//Services
import { PropertyNameChangeHelper } from '@app/shared/services/property-name-change.service';
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, helper) {
        this.http = http;
        this.helper = helper;
        this.userBS = new BehaviorSubject(this.user);
        try {
            this.currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
            this.currentUser = this.currentUserSubject.asObservable();
        }
        catch (_a) {
            // localStorage.removeItem('currentUser');
        }
    }
    AuthenticationService.prototype.checkCurrentUser = function () {
        var _this = this;
        return this.http.get(environment.apiUrl + "/auth/verify")
            .pipe(map(function (user) {
            user.userModel = _this.helper.changeKeyName(user.userModel, 'id', '_id');
            return user;
        }));
    };
    AuthenticationService.prototype.login = function (username, password) {
        return this.http.post(environment.apiUrl + "/auth/login", { username: username, password: password })
            .pipe(map(function (user) {
            if (user) {
                localStorage.setItem('currentUser', user.accessToken);
            }
            return user;
        }));
    };
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem('currentUser');
    };
    AuthenticationService.prototype.getCurrentUser = function () {
        return this.userBS.asObservable();
    };
    AuthenticationService.prototype.dispatch = function (user) {
        this.next(user);
    };
    AuthenticationService.prototype.next = function (user) {
        this.user = user;
        this.userBS.next(user);
    };
    AuthenticationService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            PropertyNameChangeHelper])
    ], AuthenticationService);
    return AuthenticationService;
}());
export { AuthenticationService };
//# sourceMappingURL=authentication.service.js.map