import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
//Environment
import { environment } from '@environments/environment';
//Services
import { PropertyNameChangeHelper } from '@app/shared/services/property-name-change.service';
import { themeStylesConst } from '@environments/theme-styles';
var UserService = /** @class */ (function () {
    function UserService(http, helper) {
        this.http = http;
        this.helper = helper;
        this.colorTheme = themeStylesConst.normal;
        this.colorThemeBS = new BehaviorSubject(this.colorTheme);
        if (localStorage.getItem('themeStyle')) {
            this.colorThemeBS.next(JSON.parse(localStorage.getItem('themeStyle')));
        }
    }
    UserService.prototype.checkUserPassword = function (userModel, password) {
        password.userModel = userModel;
        return this.http.post(environment.apiUrl + "/users/changeUserPassword", password);
    };
    UserService.prototype.getAll = function () {
        var _this = this;
        return this.http.get(environment.apiUrl + "/users")
            .pipe(map(function (user) {
            user = _this.helper.changeKeyNameFromArray(user, 'id', '_id');
            return user;
        }));
    };
    UserService.prototype.getById = function (id) {
        var _this = this;
        return this.http.get(environment.apiUrl + "/users/" + id)
            .pipe(map(function (user) {
            user = _this.helper.changeKeyName(user, 'id', '_id');
            return user;
        }));
    };
    UserService.prototype.register = function (user) {
        var _this = this;
        return Observable.create(function (observer) {
            _this.http.post(environment.apiUrl + "/users/", user).subscribe(function (createdUser) {
                if (createdUser) {
                    observer.next(createdUser);
                    observer.complete();
                }
                else {
                    observer.error('User already exist');
                }
            }, function (err) {
                observer.error('User already exist');
                throw err;
            });
        });
    };
    UserService.prototype.update = function (user) {
        var _this = this;
        return this.http.put(environment.apiUrl + "/users/" + user.id, user)
            .pipe(map(function (user) {
            user = _this.helper.changeKeyName(user, 'id', '_id');
            return user;
        }));
    };
    UserService.prototype.delete = function (id) {
        var _this = this;
        return this.http.delete(environment.apiUrl + "/users/" + id)
            .pipe(map(function (user) {
            user = _this.helper.changeKeyName(user, 'id', '_id');
            return user;
        }));
    };
    UserService.prototype.getTotalElements = function () {
        return this.http.get(environment.apiUrl + "/count/");
    };
    UserService.prototype.getUsersForPagination = function (currentPage, count) {
        var _this = this;
        return this.http.get(environment.apiUrl + "/users/?page=" + currentPage + "&_limit=" + count)
            .pipe(map(function (user) {
            user = _this.helper.changeKeyNameFromArray(user, 'id', '_id');
            return user;
        }));
    };
    UserService.prototype.setColorTheme = function (color) {
        this.colorTheme = color;
        this.colorThemeBS.next(color);
        localStorage.setItem('themeStyle', JSON.stringify(color));
    };
    UserService.prototype.getColorTheme = function () {
        return this.colorThemeBS.asObservable();
    };
    UserService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            PropertyNameChangeHelper])
    ], UserService);
    return UserService;
}());
export { UserService };
//# sourceMappingURL=user.service.js.map