import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from "@angular/core";
import { Subject, fromEvent } from "rxjs";
var LayoutService = /** @class */ (function () {
    function LayoutService() {
        var _this = this;
        this.mouseEvent = new Subject();
        fromEvent(document.body, 'click').subscribe(function (event) {
            _this.mouseEvent.next(event);
        });
    }
    LayoutService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [])
    ], LayoutService);
    return LayoutService;
}());
export { LayoutService };
//# sourceMappingURL=layout.service.js.map