import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from '@app/shared/services';
//Models
var ChatService = /** @class */ (function () {
    function ChatService(socket, auth) {
        this.socket = socket;
        this.auth = auth;
        this.statusChat = { statusWindow: true, online: 0, message: '' };
        this.statusChatBS = new BehaviorSubject(this.statusChat);
        this.messageArray = [];
        this.messageArrayBS = new BehaviorSubject(this.messageArray);
    }
    ChatService.prototype.getMessage = function () {
        return this.socket.fromEvent('events');
    };
    ChatService.prototype.getTotalOnlineUsers = function () {
        return this.socket.fromEvent('users');
    };
    ChatService.prototype.sendMessage = function (msg) {
        var _this = this;
        var time = new Date().toTimeString().split(' ')[0];
        var currentUser = this.auth.getCurrentUser().subscribe(function (data) {
            _this.socket.emit('events', { username: data.username, message: msg, time: time }, function (data) {
                currentUser.unsubscribe();
            });
        });
    };
    // public onSocketOn(): void {
    //     return this.socket.on('events', data => console.log(data));
    // }
    ChatService.prototype.setMessageArray = function (messages) {
        this.messageArray = messages;
        this.messageArrayBS.next(messages);
    };
    ChatService.prototype.getMessageArray = function () {
        return this.messageArrayBS.asObservable();
    };
    ChatService.prototype.getChatStatus = function () {
        return this.statusChatBS.asObservable();
    };
    ChatService.prototype.setChatOptions = function (status, online) {
        this.statusChat.statusWindow = status;
        this.statusChat.online = online;
        this.statusChatBS.next(this.statusChat);
    };
    ChatService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [Socket,
            AuthenticationService])
    ], ChatService);
    return ChatService;
}());
export { ChatService };
//# sourceMappingURL=chat.service.js.map