import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
var MenuService = /** @class */ (function () {
    function MenuService(menu) {
        this.menu = menu;
        this.menuIsOpenBS = new BehaviorSubject(this.menuIsOpen);
    }
    MenuService.prototype.openMenuEvent = function () {
        //   if (this.menuIsOpen) {
        //     this.menuIsOpen = false;
        //     this.menu.close('first');
        //   } else {        
        // this.menuIsOpen = true;
        this.menu.open('first');
        this.menuIsOpen = true;
        this.menuIsOpenBS.next(true);
        //   }
    };
    MenuService.prototype.closeMenuEvent = function () {
        this.menu.close('first');
        this.menuIsOpenBS.next(false);
        // this.menuIsOpen = false;
    };
    MenuService.prototype.getMenuStatus = function () {
        return this.menuIsOpenBS.asObservable();
    };
    MenuService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [MenuController])
    ], MenuService);
    return MenuService;
}());
export { MenuService };
//# sourceMappingURL=menu.service.js.map