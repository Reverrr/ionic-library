import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
//Services
import { AuthenticationService } from '@app/shared/services';
var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor(authenticationService) {
        this.authenticationService = authenticationService;
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        // add authorization header with jwt token if available
        if (localStorage.getItem('currentUser')) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + localStorage.getItem('currentUser')
                }
            });
        }
        return next.handle(request);
    };
    JwtInterceptor = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [AuthenticationService])
    ], JwtInterceptor);
    return JwtInterceptor;
}());
export { JwtInterceptor };
//# sourceMappingURL=jwt.interceptor.js.map