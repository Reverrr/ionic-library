import * as tslib_1 from "tslib";
//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
// import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//Components
// import { HeaderComponent } from "@app/shared/containers/header/header.component";
// import { FooterComponent } from "@app/shared/containers/footer/footer.component";
import { DropDownAuthorsComponent } from "@app/shared/components/drop-down-authors/drop-down-authors.component";
import { NgxPaginationModule } from 'ngx-pagination';
//Pipes
// import { DescriptionCutPipe } from "@app/shared/pipes/description-cut.pipe";
// import { OrderByPipe } from "@app/shared/pipes/order-by.pipe";
//Modules
// import { ChatModule } from "@app/chat/chat.module";
import { HeaderComponent } from '@app/shared/header/header.component';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from '@app/shared/components/menu-component/menu.component';
import { PopoverPage } from './components/popover/popover.component';
import { SliderComponent } from './components/sliders/sliders.component';
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                // HeaderComponent,
                // FooterComponent,
                DropDownAuthorsComponent,
                HeaderComponent,
                MenuComponent,
                PopoverPage,
                SliderComponent,
            ],
            imports: [
                IonicModule,
                // BrowserModule,
                CommonModule,
                FormsModule,
                NgxPaginationModule,
                // ChatModule,
                // NgxPaginationModule,
                // Ng2SearchPipeModule,
                ReactiveFormsModule,
            ],
            exports: [
                // HeaderComponent,
                // FooterComponent,
                // ChatModule,
                DropDownAuthorsComponent,
                HeaderComponent,
                MenuComponent,
                PopoverPage,
                SliderComponent,
                CommonModule,
                NgxPaginationModule,
                FormsModule,
            ]
        })
    ], SharedModule);
    return SharedModule;
}());
export { SharedModule };
//# sourceMappingURL=shared.module.js.map