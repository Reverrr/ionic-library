import * as tslib_1 from "tslib";
//Vendors
import { Component, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { AlertController } from '@ionic/angular';
//Services
import { LayoutService, LibraryService } from "@app/shared/services";
//Models
import { AuthorModel } from "@app/shared/models";
var DropDownAuthorsComponent = /** @class */ (function () {
    function DropDownAuthorsComponent(layout, libraryService, alertController) {
        this.layout = layout;
        this.libraryService = libraryService;
        this.alertController = alertController;
        this.selectedAuthors = [];
        this.authors = [];
        this.allAuthorsClone = [];
        this.newAuthor = new AuthorModel();
        this.ionAllAuthors = [];
        this.onChanged = new EventEmitter();
        this.addAuthorForm = new FormGroup({
            author: new FormControl('', []),
            selectAuthor: new FormControl('', []),
        });
        this.getAuthors();
    }
    DropDownAuthorsComponent.prototype.onSelect = function (event) {
        event.stopPropagation();
    };
    DropDownAuthorsComponent.prototype.presentAlert = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Select Authors',
                            inputs: this.ionAllAuthors,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                }, {
                                    text: 'Ok',
                                    handler: function (event) {
                                        _this.ionAllAuthors.forEach(function (item) { return item.checked = false; });
                                        event.forEach(function (element, index) {
                                            var indexAuthorSelect = _this.ionAllAuthors.findIndex(function (i) { return i.name === element; });
                                            _this.ionAllAuthors[indexAuthorSelect].checked = true;
                                        });
                                        _this.selectedAuthors = event;
                                        _this.onChanged.emit(_this.selectedAuthors);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // public selectedOption(index: number): void {
    //     this.selectedAuthors.push(this.authors[index].name);
    //     this.authors.splice(index, 1);
    //     this.onChanged.emit(this.selectedAuthors);
    // }
    // public removeAuthor(option: string, index: number): void {
    //     const indexInCloneAuthor = this.allAuthorsClone.findIndex(i => i.name === option);
    //     this.selectedAuthors.splice(index, 1);
    //     this.authors.push(this.allAuthorsClone[indexInCloneAuthor]);
    //     this.onChanged.emit(this.selectedAuthors);
    // }
    DropDownAuthorsComponent.prototype.getAuthors = function () {
        var _this = this;
        this.libraryService.getAllAuthors().subscribe(function (allAuthors) {
            _this.authors = allAuthors;
            _this.allAuthorsClone = allAuthors.slice();
            allAuthors.forEach(function (item) {
                _this.ionAllAuthors.push({
                    name: item.name,
                    type: 'checkbox',
                    label: item.name,
                    value: item.name,
                    checked: false
                });
            });
        });
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], DropDownAuthorsComponent.prototype, "onChanged", void 0);
    DropDownAuthorsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-drop-down-authors',
            templateUrl: './drop-down-authors.component.html',
            styleUrls: ['./drop-down-authors.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LayoutService,
            LibraryService,
            AlertController])
    ], DropDownAuthorsComponent);
    return DropDownAuthorsComponent;
}());
export { DropDownAuthorsComponent };
//# sourceMappingURL=drop-down-authors.component.js.map