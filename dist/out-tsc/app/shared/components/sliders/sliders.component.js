import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var SliderComponent = /** @class */ (function () {
    function SliderComponent() {
        this.slideOpts = {
            initialSlide: 0,
            speed: 300,
            loop: true,
            autoplay: {
                delay: 1000,
            },
        };
    }
    SliderComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sliders',
            templateUrl: './sliders.component.html',
            styleUrls: ['./sliders.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SliderComponent);
    return SliderComponent;
}());
export { SliderComponent };
//# sourceMappingURL=sliders.component.js.map