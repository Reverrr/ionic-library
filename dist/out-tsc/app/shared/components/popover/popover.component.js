import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var PopoverPage = /** @class */ (function () {
    function PopoverPage() {
    }
    PopoverPage.prototype.close = function () {
        return 0;
    };
    PopoverPage = tslib_1.__decorate([
        Component({
            templateUrl: './popover.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], PopoverPage);
    return PopoverPage;
}());
export { PopoverPage };
//# sourceMappingURL=popover.component.js.map