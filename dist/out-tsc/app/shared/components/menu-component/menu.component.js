import * as tslib_1 from "tslib";
//Vendors
import { Component } from "@angular/core";
import { MenuService } from '../../services/menu.service';
var MenuComponent = /** @class */ (function () {
    function MenuComponent(menuService) {
        var _this = this;
        this.menuService = menuService;
        this.toggled = false;
        this.menuService.getMenuStatus().subscribe(function (status) {
            _this.menuIsOpen = status;
        });
    }
    MenuComponent.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    MenuComponent.prototype.cancelSearch = function (event) {
        this.toggled = false;
    };
    MenuComponent.prototype.openFirst = function () {
        this.menuService.openMenuEvent();
    };
    MenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-menu',
            templateUrl: './menu.component.html',
            styleUrls: ['./menu.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [MenuService])
    ], MenuComponent);
    return MenuComponent;
}());
export { MenuComponent };
//# sourceMappingURL=menu.component.js.map