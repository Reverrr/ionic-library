var ErrorModel = /** @class */ (function () {
    function ErrorModel() {
        this.description = 'Unhandled Error';
    }
    return ErrorModel;
}());
export { ErrorModel };
//# sourceMappingURL=error.model.js.map