import * as tslib_1 from "tslib";
//Vendors
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
//Services
import { AuthenticationService } from '@app/shared/services/authentication.service';
var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, authenticationService) {
        this.router = router;
        this.authenticationService = authenticationService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var _this = this;
        return true;
        return Observable.create(function (observer) {
            _this.authenticationService.checkCurrentUser().subscribe(function (data) {
                if (!data) {
                    _this.authenticationService.logout();
                    return false;
                }
                localStorage.setItem('currentUser', data.newToken);
                _this.authenticationService.dispatch(data.userModel);
                if (route.data.role.indexOf(data.userModel.role) !== -1) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    _this.router.navigate(['/']);
                    observer.complete();
                }
            }, function (error) {
                if (state.url === '/auth/login' || state.url === '/auth/register') {
                    observer.next(true);
                }
                else {
                    observer.next(false);
                    _this.router.navigate(['/auth/login']);
                }
            });
        });
    };
    AuthGuard = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [Router,
            AuthenticationService])
    ], AuthGuard);
    return AuthGuard;
}());
export { AuthGuard };
//# sourceMappingURL=auth.guard.js.map