export var BookTypeEnum;
(function (BookTypeEnum) {
    BookTypeEnum["all"] = "all";
    BookTypeEnum["book"] = "book";
    BookTypeEnum["magazine"] = "magazine";
})(BookTypeEnum || (BookTypeEnum = {}));
;
//# sourceMappingURL=book-type.enum.js.map