import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'library',
                children: [
                    {
                        path: '',
                        loadChildren: '../library/library.module#LibraryModule'
                    }
                ]
            },
            {
                path: 'admin',
                children: [
                    {
                        path: '',
                        loadChildren: '../admin/admin.module#AdminModule'
                    }
                ]
            },
            {
                path: 'chat',
                children: [
                    {
                        path: '',
                        loadChildren: '../chat/chat.module#ChatModule'
                    }
                ]
            },
            {
                path: 'profile',
                children: [
                    {
                        path: '',
                        loadChildren: '../profile/profile.module#ProfileModule'
                    }
                ]
            },
            {
                path: 'camera',
                children: [
                    {
                        path: '',
                        loadChildren: '../camera/camera.module#CameraModule'
                    }
                ]
            },
            {
                path: 'geolocation',
                children: [
                    {
                        path: '',
                        loadChildren: '../geolocation/geolocation.module#GeolocationModule'
                    }
                ]
            },
            {
                path: 'gallery',
                children: [
                    {
                        path: '',
                        loadChildren: '../gallery-images/gallery-images.module#GalleryImagesModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/library',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/library',
        pathMatch: 'full'
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes),
                HttpClientModule
            ],
            exports: [RouterModule]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());
export { TabsPageRoutingModule };
//# sourceMappingURL=tabs.router.module.js.map