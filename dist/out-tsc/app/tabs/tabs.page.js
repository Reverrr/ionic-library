import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/shared/services';
import { ChatService } from '../shared/services/chat.service';
import { MenuService } from '@app/shared/services/menu.service';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../shared/components/popover/popover.component';
import { RoleEnum } from '@app/shared/enums';
var TabsPage = /** @class */ (function () {
    function TabsPage(router, authenticationService, chatService, menuService, popoverController) {
        var _this = this;
        this.router = router;
        this.authenticationService = authenticationService;
        this.chatService = chatService;
        this.menuService = menuService;
        this.popoverController = popoverController;
        this.unreadMessages = 0;
        this.animationsUnreadMessages = false;
        this.isAdmin = true;
        this.chatService.getMessage().subscribe(function (message) {
            if (_this.router.url !== '/tabs/chat') {
                _this.unreadMessages++;
                _this.animationsUnreadMessages = false;
                setTimeout(function () {
                    _this.animationsUnreadMessages = true;
                }, 100);
            }
            else {
                _this.unreadMessages = 0;
            }
        });
        this.authenticationService.checkCurrentUser().subscribe(function (user) {
            if (user.userModel.role === RoleEnum.admin) {
                _this.isAdmin = true;
            }
        });
    }
    TabsPage.prototype.presentPopover = function (ev) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var popover;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverController.create({
                            component: PopoverPage,
                            event: ev,
                            translucent: true
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TabsPage.prototype.clearUnreadMessage = function () {
        this.unreadMessages = 0;
    };
    TabsPage.prototype.closeMenu = function () {
        this.menuService.closeMenuEvent();
    };
    TabsPage.prototype.logout = function () {
        this.authenticationService.logout();
        this.router.navigate(['auth/login']);
    };
    TabsPage = tslib_1.__decorate([
        Component({
            selector: 'app-tabs',
            templateUrl: 'tabs.page.html',
            styleUrls: ['tabs.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            AuthenticationService,
            ChatService,
            MenuService,
            PopoverController])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs.page.js.map