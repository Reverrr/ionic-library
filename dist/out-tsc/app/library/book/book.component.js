import * as tslib_1 from "tslib";
//Vendors
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//Services
import { LibraryService } from "@app/shared/services/library.service";
//Environment
import { environment } from "@environments/environment";
import { Location } from '@angular/common';
var BookComponent = /** @class */ (function () {
    function BookComponent(route, libraryService, router, location) {
        this.route = route;
        this.libraryService = libraryService;
        this.router = router;
        this.location = location;
        this.getSelectedBook();
        if (!!this.subscribeToGetBook) {
            this.subscribeToGetBook.unsubscribe();
        }
    }
    BookComponent.prototype.ngOnInit = function () { };
    BookComponent.prototype.getSelectedBook = function () {
        var _this = this;
        this.subscribeToGetBook = this.libraryService.getSelectedBook().subscribe(function (data) {
            if (!data) {
                _this.libraryService.getBookById(_this.route.snapshot.params.id).subscribe(function (book) {
                    if (book.img) {
                        _this.book = book;
                    }
                    else {
                        _this.router.navigateByUrl("/");
                    }
                }, function (err) { throw err; });
            }
            else {
                _this.book = data;
            }
        }, function (err) { throw err; });
    };
    BookComponent.prototype.updateUrl = function () {
        this.book.img = environment.defaultBookImgUrl;
    };
    BookComponent.prototype.returnPrevPage = function () {
        this.location.back();
    };
    BookComponent = tslib_1.__decorate([
        Component({
            selector: 'app-book',
            templateUrl: './book.component.html',
            styleUrls: ['./book.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute,
            LibraryService,
            Router,
            Location])
    ], BookComponent);
    return BookComponent;
}());
export { BookComponent };
//# sourceMappingURL=book.component.js.map