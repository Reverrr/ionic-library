import * as tslib_1 from "tslib";
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LibraryComponent } from './library/library.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '@app/shared/shared.module';
import { BookComponent } from './book/book.component';
var LibraryModule = /** @class */ (function () {
    function LibraryModule() {
    }
    LibraryModule = tslib_1.__decorate([
        NgModule({
            imports: [
                IonicModule,
                CommonModule,
                FormsModule,
                SharedModule,
                NgxPaginationModule,
                RouterModule.forChild([
                    {
                        path: '', component: LibraryComponent
                    },
                    {
                        path: ':id',
                        component: BookComponent
                    }
                ])
            ],
            declarations: [
                LibraryComponent,
                BookComponent
            ]
        })
    ], LibraryModule);
    return LibraryModule;
}());
export { LibraryModule };
//# sourceMappingURL=library.module.js.map