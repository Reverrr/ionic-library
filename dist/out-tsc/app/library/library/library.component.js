import * as tslib_1 from "tslib";
//Vendors
import { Component, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
//Services
import { LibraryService } from "@app/shared/services/library.service";
//Environment
import { environment } from "@environments/environment";
import { BookTypeEnum } from "@app/shared/enums/book-type.enum";
import { IonInfiniteScroll, IonContent, AlertController, PopoverController } from '@ionic/angular';
var LibraryComponent = /** @class */ (function () {
    function LibraryComponent(libraryService, router, alertController, popoverController) {
        this.libraryService = libraryService;
        this.router = router;
        this.alertController = alertController;
        this.popoverController = popoverController;
        this.filterArrayAuthors = [];
        this.selectedTypeBook = '';
        this.currentPage = 1;
        this.valueSearch = '';
        this.type = BookTypeEnum;
        this.toggled = false;
        this.showSearch = false;
        console.log('AAAAAAAAAAAAAAA!!!!!!!!!!!!!!!!!');
        this.toggled = false;
        // this.libraryService.getTotalElements().subscribe(
        //     (data: CountModel) => {
        //         this.totalCountBooks = data[0].totalCountBooks;
        //     },
        //     (err: ErrorModel<string, number>) => { throw err }
        // );
        this.loadAllBooks();
    }
    LibraryComponent.prototype.ngOnInit = function () { };
    LibraryComponent.prototype.presentAlert = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Select Type Book',
                            inputs: [
                                {
                                    name: BookTypeEnum.all,
                                    type: 'radio',
                                    label: BookTypeEnum.all,
                                    value: ''
                                },
                                {
                                    name: BookTypeEnum.book,
                                    type: 'radio',
                                    label: BookTypeEnum.book,
                                    value: BookTypeEnum.book
                                },
                                {
                                    name: BookTypeEnum.magazine,
                                    type: 'radio',
                                    label: BookTypeEnum.magazine,
                                    value: BookTypeEnum.magazine
                                },
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () { }
                                }, {
                                    text: 'Ok',
                                    handler: function (event) {
                                        _this.selectedTypeBook = event;
                                        _this.getSearchBooks(_this.valueSearch);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LibraryComponent.prototype.toogleSearch = function () {
        this.showSearch = !this.showSearch;
    };
    LibraryComponent.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    LibraryComponent.prototype.cancelSearch = function (event) {
        this.toggled = false;
    };
    LibraryComponent.prototype.logScrollStart = function () {
        var _this = this;
        this.content.scrollToTop(1500).then(function (data) {
            _this.scrollToTop = false;
        });
    };
    LibraryComponent.prototype.loadData = function (event) {
        var _this = this;
        this.currentPage++;
        this.scrollToTop = true;
        this.libraryService.searchBooks(this.valueSearch, 6, this.filterArrayAuthors, (this.selectedTypeBook === this.type.all) ? '' : this.selectedTypeBook, this.currentPage).subscribe(function (data) {
            var _a;
            event.target.complete();
            event.target.disabled = false;
            (_a = _this.books).push.apply(_a, data);
        }, function (err) { throw err; });
    };
    LibraryComponent.prototype.toggleInfiniteScroll = function () {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    };
    LibraryComponent.prototype.searchEvent = function (event) {
        var e = event.target;
        this.valueSearch = e.value;
        this.getSearchBooks(this.valueSearch);
    };
    LibraryComponent.prototype.changeCurrentPage = function (page) {
        var _this = this;
        this.currentPage = page;
        this.libraryService.searchBooks(this.valueSearch, 6, this.filterArrayAuthors, (this.selectedTypeBook === this.type.all) ? '' : this.selectedTypeBook, this.currentPage).subscribe(function (data) {
            _this.books = data;
        }, function (err) { throw err; });
    };
    LibraryComponent.prototype.onChanged = function (authors) {
        this.filterArrayAuthors = authors;
        this.getSearchBooks(this.valueSearch);
    };
    LibraryComponent.prototype.checkTypeSelected = function (event) {
        var e = event.target;
        this.selectedTypeBook = e.value;
        this.getSearchBooks(this.valueSearch);
    };
    LibraryComponent.prototype.getSearchBooks = function (valueSearch) {
        var _this = this;
        if (!valueSearch && !this.filterArrayAuthors.length && !this.selectedTypeBook || this.selectedTypeBook === 'all' && !valueSearch && !this.filterArrayAuthors.length) {
            this.searchingStatus = false;
        }
        else {
            this.searchingStatus = true;
            this.currentPage = 1;
        }
        this.valueSearch = valueSearch;
        this.libraryService.searchBooks(valueSearch, 6, this.filterArrayAuthors, (this.selectedTypeBook === this.type.all) ? '' : this.selectedTypeBook, this.currentPage)
            .subscribe(function (data) {
            _this.books = data;
        }, function (err) { throw err; });
    };
    LibraryComponent.prototype.updateUrl = function (book) {
        book.img = environment.defaultBookImgUrl;
    };
    LibraryComponent.prototype.loadAllBooks = function () {
        var _this = this;
        this.libraryService.searchBooks(this.valueSearch, 6, this.filterArrayAuthors, (this.selectedTypeBook === this.type.all) ? '' : this.selectedTypeBook, this.currentPage)
            .subscribe(function (data) {
            _this.books = data;
        }, function (err) { throw err; });
    };
    LibraryComponent.prototype.selectBook = function (index) {
        this.libraryService.dispatch(this.books[index]);
        this.router.navigateByUrl("/tabs/library/" + this.books[index].id);
    };
    tslib_1.__decorate([
        ViewChild(IonInfiniteScroll),
        tslib_1.__metadata("design:type", IonInfiniteScroll)
    ], LibraryComponent.prototype, "infiniteScroll", void 0);
    tslib_1.__decorate([
        ViewChild(IonContent),
        tslib_1.__metadata("design:type", IonContent)
    ], LibraryComponent.prototype, "content", void 0);
    LibraryComponent = tslib_1.__decorate([
        Component({
            selector: 'app-libary',
            templateUrl: './library.component.html',
            styleUrls: ['./library.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LibraryService,
            Router,
            AlertController,
            PopoverController])
    ], LibraryComponent);
    return LibraryComponent;
}());
export { LibraryComponent };
//# sourceMappingURL=library.component.js.map