import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserService } from './shared/services/user.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, toastController, userService) {
        var _this = this;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.toastController = toastController;
        this.userService = userService;
        this.userService.getColorTheme().subscribe(function (color) {
            document.documentElement.style.setProperty("--ion-color-primary", color.main);
            document.documentElement.style.setProperty("--ion-color-primary-shade", color.shade);
            document.documentElement.style.setProperty("--ion-color-primary-tint", color.tint);
        });
        this.initializeApp();
        this.platform.ready().then(function (data) {
            console.log(data);
            if (_this.platform.is('cordova')) {
                // make your native API calls
            }
            else {
                // fallback to browser APIs
            }
        });
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            ToastController,
            UserService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map