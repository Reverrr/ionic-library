import * as tslib_1 from "tslib";
//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { SocketIoModule } from 'ngx-socket-io';
import { MainChatComponent } from '@app/chat/main-chat/main-chat.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { IonicModule } from '@ionic/angular';
//Routing
var config = { url: 'http://localhost:8077', options: {} };
var ChatModule = /** @class */ (function () {
    function ChatModule() {
    }
    ChatModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                MainChatComponent
            ],
            imports: [
                // BrowserModule,
                CommonModule,
                SharedModule,
                IonicModule,
                ReactiveFormsModule,
                SocketIoModule.forRoot(config),
                RouterModule.forChild([{ path: '', component: MainChatComponent }]),
            ],
            exports: [
                MainChatComponent
            ]
        })
    ], ChatModule);
    return ChatModule;
}());
export { ChatModule };
//# sourceMappingURL=chat.module.js.map