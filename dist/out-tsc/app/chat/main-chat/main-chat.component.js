import * as tslib_1 from "tslib";
//Vendors
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ChatService } from '../../shared/services/chat.service';
import { AuthenticationService } from '@app/shared/services';
//Services
//Model
var MainChatComponent = /** @class */ (function () {
    function MainChatComponent(socket, authService) {
        this.socket = socket;
        this.authService = authService;
        this.message = '';
        this.toggleMessageBool = false;
        this.messages = [];
        this.unreadMessages = 0;
        this.animationsUnreadMessages = false;
        this.editFormGroup = new FormGroup({
            message: new FormControl('', [Validators.required, Validators.min(1)]),
        });
    }
    MainChatComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socket.getChatStatus().subscribe(function (status) {
            _this.toggleMessageBool = status.statusWindow;
            _this.onlineUsers = status.online;
        });
        this.getMessageSubscribe = this.socket.getMessage().subscribe(function (message) {
            _this.curentUserSubscribe = _this.authService.getCurrentUser().subscribe(function (data) {
                if (!!_this.curentUserSubscribe) {
                    _this.curentUserSubscribe.unsubscribe();
                }
                if (message.username === data.username) {
                    message.color = true;
                }
                _this.messages.push(message);
                _this.scrollToBottom();
                if (!_this.toggleMessageBool) {
                    _this.unreadMessages++;
                    _this.animationsUnreadMessages = false;
                    setTimeout(function () {
                        _this.animationsUnreadMessages = true;
                    }, 100);
                }
                else {
                    _this.unreadMessages = 0;
                }
            });
        });
        this.subscribeOnlineUsers = this.socket.getTotalOnlineUsers().subscribe(function (data) {
            _this.onlineUsers = data;
            _this.socket.setChatOptions(_this.toggleMessageBool, data);
        });
        this.socket.getMessageArray().subscribe(function (data) {
            _this.messages = data;
        });
    };
    MainChatComponent.prototype.ngOnDestroy = function () {
        this.getMessageSubscribe.unsubscribe();
        this.subscribeOnlineUsers.unsubscribe();
        this.socket.setMessageArray(this.messages);
    };
    MainChatComponent.prototype.ngAfterViewChecked = function () {
        this.scrollToBottom();
    };
    MainChatComponent.prototype.scrollToBottom = function () {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
        catch (err) { }
    };
    MainChatComponent.prototype.sendMessage = function (event) {
        if (this.editFormGroup.valid) {
            this.socket.sendMessage(this.editFormGroup.controls.message.value);
            this.editFormGroup.reset();
        }
    };
    MainChatComponent.prototype.toggleMessage = function () {
        this.toggleMessageBool = !this.toggleMessageBool;
        this.unreadMessages = 0;
        this.socket.setChatOptions(this.toggleMessageBool, this.onlineUsers);
    };
    tslib_1.__decorate([
        ViewChild('scroll'),
        tslib_1.__metadata("design:type", ElementRef)
    ], MainChatComponent.prototype, "myScrollContainer", void 0);
    MainChatComponent = tslib_1.__decorate([
        Component({
            selector: 'app-main-chat',
            templateUrl: './main-chat.component.html',
            styleUrls: ['./main-chat.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ChatService,
            AuthenticationService])
    ], MainChatComponent);
    return MainChatComponent;
}());
export { MainChatComponent };
//# sourceMappingURL=main-chat.component.js.map