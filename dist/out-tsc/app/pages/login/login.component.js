import * as tslib_1 from "tslib";
//Vendors
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { FormBuilder, Validators } from '@angular/forms';
//Services
import { AuthenticationService } from '@app/shared/services';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, route, router, authenticationService) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.loading = false;
        this.submitted = false;
        if (localStorage.getItem('currentUser')) {
            this.router.navigate(['/']);
        }
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    LoginComponent.prototype.redirectToReg = function () {
        this.router.navigateByUrl('/register');
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () {
            return this.loginForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        /**
         * Temp for Android
         */
        this.router.navigate([this.returnUrl]);
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(function (data) {
            if (data) {
                _this.failToGetData = false;
                _this.router.navigate([_this.returnUrl]);
                _this.loading = false;
            }
            else {
                _this.failToGetData = true;
                _this.loading = false;
            }
        }, function (error) {
            _this.router.navigate(['/registration']);
            _this.loading = false;
        });
    };
    LoginComponent = tslib_1.__decorate([
        Component({ templateUrl: 'login.component.html' }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder,
            ActivatedRoute,
            Router,
            AuthenticationService])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map