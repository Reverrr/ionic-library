import * as tslib_1 from "tslib";
//Vendors
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';
import { AuthenticationService } from '@app/shared/services/authentication.service';
import { UserService } from '@app/shared/services';
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(formBuilder, router, authenticationService, userService, location) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.router = router;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.location = location;
        this.loading = false;
        this.submitted = false;
        this.isUserNameExist = false;
        this.currentUrl = '';
        if (!!this.subscribe) {
            this.subscribe.unsubscribe();
        }
        this.subscribe = this.authenticationService.getCurrentUser().subscribe(function (data) {
            _this.user = data;
            if (!!_this.subscribe) {
                _this.subscribe.unsubscribe();
            }
        });
        this.currentUrl = this.router.url;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.registerForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern("^[a-zA-Z]*$")]],
            lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern("^[a-zA-Z]*$")]],
            username: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(18), Validators.pattern("^[[A-Za-z0-9]+]*$")]],
            password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]],
            role: ['user', [Validators.required]]
        });
    };
    Object.defineProperty(RegisterComponent.prototype, "f", {
        get: function () {
            return this.registerForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    RegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        this.loading = true;
        this.subscribe = this.userService.register(this.registerForm.value).pipe(first()).subscribe(function (data) {
            _this.isUserNameExist = false;
            if (_this.router.url === '/register') {
                _this.router.navigate(['/login']);
                _this.subscribe.unsubscribe();
            }
            else {
                _this.location.back();
                _this.subscribe.unsubscribe();
            }
        }, function (error) {
            _this.isUserNameExist = true;
            _this.loading = false;
        });
    };
    RegisterComponent.prototype.redirectToLogin = function () {
        if (this.router.url === '/tabs/admin/admin/edit-users/add-new-user') {
            this.router.navigate(['/tabs/admin/admin/edit-users/']);
        }
        else {
            this.location.back();
        }
    };
    RegisterComponent = tslib_1.__decorate([
        Component({
            selector: 'app-register',
            templateUrl: 'register.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder,
            Router,
            AuthenticationService,
            UserService,
            Location])
    ], RegisterComponent);
    return RegisterComponent;
}());
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map