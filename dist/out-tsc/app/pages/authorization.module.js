import * as tslib_1 from "tslib";
//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { LoginComponent } from "@app/pages/login";
import { RegisterComponent } from "@app/pages/register";
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
var routes = [
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: '',
        redirectTo: '/auth/register',
        pathMatch: 'full'
    }
];
var AuthorizationModule = /** @class */ (function () {
    function AuthorizationModule() {
    }
    AuthorizationModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                RegisterComponent,
                LoginComponent
            ],
            imports: [
                IonicModule,
                CommonModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            exports: [
                RouterModule,
                RegisterComponent,
            ]
        })
    ], AuthorizationModule);
    return AuthorizationModule;
}());
export { AuthorizationModule };
//# sourceMappingURL=authorization.module.js.map