import * as tslib_1 from "tslib";
//Vendors
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
//Services
import { LibraryService } from '@app/shared/services/library.service';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
var SettingLibraryComponent = /** @class */ (function () {
    function SettingLibraryComponent(libraryService, router, ngxSmartModalService, alertController) {
        var _this = this;
        this.libraryService = libraryService;
        this.router = router;
        this.ngxSmartModalService = ngxSmartModalService;
        this.alertController = alertController;
        this.currentPage = 1;
        // this.libraryService.getTotalElements().subscribe(
        //     (count: CountModel) => {
        //         this.countTotalBooks = count[0].totalCountBooks;
        //     },
        //     (err: ErrorModel<string, number>) => { throw err }
        // );
        this.libraryService.getBooksForPagination(1, 10).subscribe(function (data) {
            _this.books = data;
        }, function (err) { throw err; });
    }
    SettingLibraryComponent.prototype.ngOnInit = function () { };
    SettingLibraryComponent.prototype.selectedBook = function (id) {
        this.router.navigateByUrl("tabs/admin/admin/setting/" + id);
    };
    SettingLibraryComponent.prototype.changeCurrentPage = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.libraryService.getBooksForPagination(currentPage, 10).subscribe(function (data) { _this.books = data; }, function (err) { throw err; });
    };
    SettingLibraryComponent.prototype.presentAlert = function (id, title, index) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Confirm!',
                            message: "Delete book: " + title,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Delete',
                                    cssClass: 'btn-delete',
                                    handler: function (data) {
                                        _this.deleteBook(id, index);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingLibraryComponent.prototype.loadData = function (event) {
        var _this = this;
        this.currentPage++;
        this.libraryService.getBooksForPagination(this.currentPage, 10).subscribe(function (data) {
            var _a;
            event.target.complete();
            event.target.disabled = false;
            (_a = _this.books).push.apply(_a, data);
        }, function (err) { throw err; });
    };
    SettingLibraryComponent.prototype.deleteBook = function (id, index) {
        var _this = this;
        this.countTotalBooks -= 1;
        this.libraryService.deleteBook(id).subscribe(function (data) {
            _this.books.splice(index, 1);
        }, function (err) { throw err; });
    };
    SettingLibraryComponent.prototype.selectedId = function (id) {
        this.ngxSmartModalService.resetModalData('deleteLibraryPopap');
        this.ngxSmartModalService.setModalData(id, 'deleteLibraryPopap');
    };
    tslib_1.__decorate([
        ViewChild(IonInfiniteScroll),
        tslib_1.__metadata("design:type", IonInfiniteScroll)
    ], SettingLibraryComponent.prototype, "infiniteScroll", void 0);
    SettingLibraryComponent = tslib_1.__decorate([
        Component({
            selector: 'setting-library',
            templateUrl: './setting-library.component.html',
            styleUrls: ['./setting-library.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LibraryService,
            Router,
            NgxSmartModalService,
            AlertController])
    ], SettingLibraryComponent);
    return SettingLibraryComponent;
}());
export { SettingLibraryComponent };
//# sourceMappingURL=setting-library.component.js.map