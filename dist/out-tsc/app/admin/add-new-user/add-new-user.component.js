import * as tslib_1 from "tslib";
//Vendors
import { Component } from "@angular/core";
var AddNewUserComponent = /** @class */ (function () {
    function AddNewUserComponent() {
        this.titleCompoennt = 'Add new User';
    }
    AddNewUserComponent.prototype.ngOnInit = function () { };
    AddNewUserComponent = tslib_1.__decorate([
        Component({
            selector: 'app-add-new-user',
            templateUrl: './add-new-user.component.html'
        })
    ], AddNewUserComponent);
    return AddNewUserComponent;
}());
export { AddNewUserComponent };
//# sourceMappingURL=add-new-user.component.js.map