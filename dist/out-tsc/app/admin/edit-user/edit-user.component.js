import * as tslib_1 from "tslib";
//Vendors
import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
//Services
import { AuthenticationService, UserService } from '@app/shared/services';
var EditUserComponent = /** @class */ (function () {
    function EditUserComponent(userService, route, router, location, auth) {
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.location = location;
        this.auth = auth;
        this.editFormGroup = new FormGroup({
            username: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18), Validators.pattern("^[[A-Za-z0-9]+]*$")]),
            firstName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern("^[a-zA-Z]*$")]),
            lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern("^[a-zA-Z]*$")]),
            role: new FormControl('user', []),
        });
        this.editPasswordForm = new FormGroup({
            currentPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]),
            newPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]),
        });
        this.checkCurrentUser();
        this.getUserById();
    }
    EditUserComponent.prototype.ngOnInit = function () { };
    EditUserComponent.prototype.checkCurrentUser = function () {
        var _this = this;
        this.auth.checkCurrentUser().subscribe(function (data) {
            _this.currentUser = data.userModel;
            if (!!data && data.userModel.role === 'admin') {
                _this.isCurrentUserAdmin = true;
            }
            else {
                _this.isCurrentUserAdmin = false;
            }
            if (!data) {
                _this.auth.logout();
                _this.router.navigateByUrl('/login');
            }
            if (!!data && data.userModel.id === _this.route.snapshot.params.id) {
                _this.isPageProfile = true;
            }
        });
    };
    EditUserComponent.prototype.getUserById = function () {
        var _this = this;
        this.userService.getById(this.route.snapshot.params.id).subscribe(function (data) {
            _this.user = data;
            _this.editFormGroup.setValue({
                username: _this.user.username,
                firstName: _this.user.firstName,
                lastName: _this.user.lastName,
                role: _this.user.role,
            });
        });
    };
    EditUserComponent.prototype.saveEditForm = function () {
        var _this = this;
        if (this.editFormGroup.valid) {
            for (var data in this.editFormGroup.value) {
                if (this.editFormGroup.value[data]) {
                    this.user[data] = this.editFormGroup.value[data];
                }
            }
            this.userService.update(this.user).pipe().subscribe(function (data) {
                _this.checkCurrentUser();
                _this.router.navigateByUrl('tabs/admin/admin/edit-users');
            }, function (err) {
                _this.errorMessage = 'Username already exist';
            });
        }
    };
    EditUserComponent.prototype.backToPrevPage = function () {
        this.location.back();
    };
    EditUserComponent.prototype.editPassword = function () {
        this.editPass = !this.editPass;
    };
    EditUserComponent.prototype.savePassword = function () {
        var _this = this;
        this.userService.checkUserPassword(this.user, this.editPasswordForm.value).subscribe(function (data) {
            _this.passwordError = '';
            _this.editPass = false;
            if (data.status === 201) {
                _this.user = data.data;
                _this.getUserById();
            }
        }, function (err) {
            _this.passwordError = err;
            throw err;
        });
    };
    EditUserComponent = tslib_1.__decorate([
        Component({
            selector: 'edit-user',
            templateUrl: './edit-user.component.html',
            styleUrls: ['./edit-user.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [UserService,
            ActivatedRoute,
            Router,
            Location,
            AuthenticationService])
    ], EditUserComponent);
    return EditUserComponent;
}());
export { EditUserComponent };
//# sourceMappingURL=edit-user.component.js.map