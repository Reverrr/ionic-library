import * as tslib_1 from "tslib";
//Vendors
import { Component, ViewChild, ElementRef } from "@angular/core";
import { Location } from "@angular/common";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
//Services
import { LibraryService, LayoutService } from "@app/shared/services/";
//Models
import { AuthorModel } from "@app/shared/models";
//Enums
import { BookTypeEnum } from "@app/shared/enums/book-type.enum";
var SettingBookComponent = /** @class */ (function () {
    function SettingBookComponent(route, router, layout, libraryService, location) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.layout = layout;
        this.libraryService = libraryService;
        this.location = location;
        this.type = BookTypeEnum;
        this.book = { id: undefined, name: '', img: '', description: '', type: undefined, author: [] };
        this.authors = [];
        this.allAuthorsClone = [];
        this.newAuthor = new AuthorModel();
        this.editFormGroup = new FormGroup({
            name: new FormControl('', [Validators.required]),
            img: new FormControl('', []),
            description: new FormControl('', [Validators.required]),
            type: new FormControl('', [Validators.required])
        });
        this.addAuthorForm = new FormGroup({
            author: new FormControl('', []),
            selectAuthor: new FormControl('', []),
        });
        if (!!this.route.snapshot.params.book) {
            this.title = 'Edit Book';
            this.libraryService.getBookById(this.route.snapshot.params.book).subscribe(function (data) {
                _this.img = data.img;
                _this.book = data;
                _this.editFormGroup.setValue({
                    name: _this.book.name,
                    img: _this.book.img,
                    description: _this.book.description,
                    type: _this.book.type
                });
                if (!data[0]) {
                    _this.getAuthors();
                }
            });
        }
        else if (this.route.snapshot.routeConfig.path === 'setting/add-new-book') {
            this.title = 'Create Book';
            this.getAuthors();
        }
    }
    SettingBookComponent.prototype.ngOnInit = function () { };
    SettingBookComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var element = event.target;
        this.editFormGroup.get('img').setValue(null);
        var reader = new FileReader();
        if (element.files && element.files.length > 0) {
            var file_1 = element.files[0];
            reader.readAsDataURL(file_1);
            reader.onload = function () {
                if (file_1.type === "image/png" || file_1.type === "image/jpg" || file_1.type === "image/jpeg") {
                    _this.book.img = reader.result;
                }
            };
        }
    };
    SettingBookComponent.prototype.clearFile = function () {
        this.book.img = '';
        this.editFormGroup.get('img').setValue(null);
        this.fileInput.nativeElement.value = '';
    };
    SettingBookComponent.prototype.saveEditForm = function () {
        if (this.editFormGroup.status === 'VALID' && this.book.author.length) {
            for (var data in this.editFormGroup.value) {
                if (!!this.editFormGroup.value && this.editFormGroup.value[data]) {
                    this.book[data] = this.editFormGroup.value[data];
                }
            }
            if (this.route.snapshot.routeConfig.path === 'setting/add-new-book') {
                this.libraryService.create(this.book).subscribe();
            }
            else if (!!this.route.snapshot.params.book) {
                this.libraryService.update(this.book).subscribe();
            }
            this.router.navigateByUrl('tabs/admin/admin/setting');
        }
        else {
            this.notAllInputFilled = true;
        }
    };
    SettingBookComponent.prototype.onSelect = function (event) {
        event.stopPropagation();
    };
    SettingBookComponent.prototype.onClickDropMenu = function () {
        var _this = this;
        if (this.dropMenuToggle) {
            this.layoutSubscription.unsubscribe();
        }
        this.layoutSubscription = this.layout.mouseEvent.subscribe(function () {
            _this.dropMenuToggle = false;
            _this.layoutSubscription.unsubscribe();
        }, function (err) { throw err; });
        this.dropMenuToggle = !this.dropMenuToggle;
    };
    SettingBookComponent.prototype.selectedOption = function (index) {
        this.book.author.push(this.authors[index]);
        this.authors.splice(index, 1);
    };
    SettingBookComponent.prototype.removeAuthor = function (option, index) {
        var indexInCloneAuthor = this.allAuthorsClone.findIndex(function (i) { return i.name === option; });
        this.book.author.splice(index, 1);
        if (this.allAuthorsClone[indexInCloneAuthor]) {
            this.authors.push(this.allAuthorsClone[indexInCloneAuthor]);
        }
    };
    SettingBookComponent.prototype.getAuthors = function () {
        var _this = this;
        this.libraryService.getAllAuthors().subscribe(function (allAuthors) {
            _this.authors = allAuthors;
            _this.allAuthorsClone = allAuthors.slice();
            _this.book.author.forEach(function (element) {
                var index = _this.authors.findIndex(function (i) { return i.name === element.name; });
                if (index !== -1) {
                    _this.authors.splice(index, 1);
                }
            });
        });
    };
    SettingBookComponent.prototype.backToPrevPage = function () {
        this.location.back();
    };
    tslib_1.__decorate([
        ViewChild('fileInput'),
        tslib_1.__metadata("design:type", ElementRef)
    ], SettingBookComponent.prototype, "fileInput", void 0);
    SettingBookComponent = tslib_1.__decorate([
        Component({
            selector: 'app-setting-book',
            templateUrl: './setting-book.component.html',
            styleUrls: ['./setting-book.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute,
            Router,
            LayoutService,
            LibraryService,
            Location])
    ], SettingBookComponent);
    return SettingBookComponent;
}());
export { SettingBookComponent };
//# sourceMappingURL=setting-book.component.js.map