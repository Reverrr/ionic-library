import * as tslib_1 from "tslib";
// Vendors
import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
//Services
import { LibraryService } from "@app/shared/services";
//Models
import { AuthorModel, BookModel } from "@app/shared/models";
var SettingAuthorsComponent = /** @class */ (function () {
    function SettingAuthorsComponent(libraryService) {
        var _this = this;
        this.libraryService = libraryService;
        this.book = new BookModel();
        this.authors = [];
        this.newAuthor = new AuthorModel();
        this.addAuthorForm = new FormGroup({
            name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(24)])
        });
        this.libraryService.getAllAuthors().subscribe(function (allAuthors) {
            _this.authors = allAuthors;
        });
    }
    SettingAuthorsComponent.prototype.ngOnInit = function () { };
    SettingAuthorsComponent.prototype.addAuthor = function () {
        var _this = this;
        if (this.addAuthorForm.valid) {
            this.libraryService.addNewAuthor(this.addAuthorForm.value).subscribe(function (data) {
                _this.authors.push(data);
            }, function (err) { throw err; });
        }
        this.addAuthorForm.reset();
    };
    SettingAuthorsComponent.prototype.removeFromSelect = function (event, id, index) {
        var _this = this;
        event.stopPropagation();
        this.libraryService.removeSelectedAuthor(id).subscribe(function () { _this.authors.splice(index, 1); }, function (err) { throw err; });
    };
    SettingAuthorsComponent = tslib_1.__decorate([
        Component({
            selector: 'setting-authors',
            templateUrl: './setting-authors.component.html',
            styleUrls: ['./setting-authors.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LibraryService])
    ], SettingAuthorsComponent);
    return SettingAuthorsComponent;
}());
export { SettingAuthorsComponent };
//# sourceMappingURL=setting-authors.component.js.map