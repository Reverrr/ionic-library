import * as tslib_1 from "tslib";
//Vendors
import { Component, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
// Services
import { UserService, AuthenticationService } from '@app/shared/services';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
var EditUsersComponent = /** @class */ (function () {
    function EditUsersComponent(authenticationService, userService, router, ngxSmartModalService, alertController) {
        var _this = this;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.router = router;
        this.ngxSmartModalService = ngxSmartModalService;
        this.alertController = alertController;
        this.users = [];
        this.currentPage = 1;
        this.ionPageLoad = 0;
        this.currentUserSubscription = this.authenticationService.getCurrentUser().subscribe(function (user) {
            _this.currentUser = user;
        });
        // this.userService.getTotalElements().subscribe(
        //     (count: CountModel) => {
        //         this.countTotalUsers = count[0].totalCountUsers;
        //     },
        //     (err: ErrorModel<string, number>) => {throw err}
        // );
    }
    EditUsersComponent.prototype.ngOnInit = function () {
        this.loadUsersForPagination(this.currentPage);
    };
    EditUsersComponent.prototype.presentAlert = function (id, username) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Confirm!',
                            message: "Delete user: " + username,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                    }
                                }, {
                                    text: 'Delete',
                                    cssClass: 'btn-delete',
                                    handler: function (data) {
                                        _this.deleteUser(id);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditUsersComponent.prototype.ngOnDestroy = function () {
        this.currentUserSubscription.unsubscribe();
    };
    EditUsersComponent.prototype.editUser = function (index) {
        this.router.navigate(['tabs/admin/admin/edit-users/edit-user', this.users[index].id]);
    };
    EditUsersComponent.prototype.deleteUser = function (id) {
        var _this = this;
        this.countTotalUsers -= 1;
        if (!this.countTotalUsers && this.currentPage !== 1) {
            this.currentPage -= 1;
        }
        this.userService.delete(id).pipe(first()).subscribe(function () {
            _this.countTotalUsers -= 1;
            _this.loadUsersForPagination(_this.currentPage);
        });
    };
    EditUsersComponent.prototype.loadUsersForPagination = function (currentPage) {
        var _this = this;
        this.currentPage++;
        this.userService.getUsersForPagination(currentPage, 20).subscribe(function (data) {
            _this.users = data;
        }, function (err) { throw err; });
    };
    EditUsersComponent.prototype.loadData = function (event) {
        var _this = this;
        this.currentPage++;
        this.userService.getUsersForPagination(this.currentPage, 20).subscribe(function (data) {
            var _a;
            event.target.complete();
            event.target.disabled = false;
            (_a = _this.users).push.apply(_a, data);
        }, function (err) { throw err; });
    };
    EditUsersComponent.prototype.toggleInfiniteScroll = function () {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    };
    EditUsersComponent.prototype.changeCurrentPage = function (currentPage) {
        var _this = this;
        this.currentPage = currentPage;
        this.userService.getUsersForPagination(currentPage, 20).subscribe(function (data) { _this.users = data; }, function (err) { throw err; });
    };
    EditUsersComponent.prototype.selectedId = function (id) {
        this.ngxSmartModalService.resetModalData('deleteUserPopap');
        this.ngxSmartModalService.setModalData(id, 'deleteUserPopap');
    };
    tslib_1.__decorate([
        ViewChild(IonInfiniteScroll),
        tslib_1.__metadata("design:type", IonInfiniteScroll)
    ], EditUsersComponent.prototype, "infiniteScroll", void 0);
    EditUsersComponent = tslib_1.__decorate([
        Component({
            templateUrl: 'edit-users.component.html',
            styleUrls: ['./edit-users.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [AuthenticationService,
            UserService,
            Router,
            NgxSmartModalService,
            AlertController])
    ], EditUsersComponent);
    return EditUsersComponent;
}());
export { EditUsersComponent };
//# sourceMappingURL=edit-users.component.js.map