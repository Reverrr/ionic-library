import * as tslib_1 from "tslib";
//Vendors
import { Component } from "@angular/core";
import { Router } from '@angular/router';
var AdminComponent = /** @class */ (function () {
    function AdminComponent(router) {
        this.router = router;
        this.currentUrl = '';
        this.router.events.subscribe(function (event) {
            // if (event instanceof NavigationEnd) {
            //     this.currentUrl = event.url;
            //     if (this.currentUrl === '/tabs/admin/admin/edit-authors') {
            //         this.animate = 'back';
            //     } else {
            //         this.animate = 'forward';
            //     }
            // }
        });
    }
    AdminComponent = tslib_1.__decorate([
        Component({
            selector: 'app-admin',
            templateUrl: './admin.component.html',
            styleUrls: ['./admin.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], AdminComponent);
    return AdminComponent;
}());
export { AdminComponent };
//# sourceMappingURL=admin.component.js.map