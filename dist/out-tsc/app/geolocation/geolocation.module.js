import * as tslib_1 from "tslib";
//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { GeolocationComponent } from './geolocation.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
var routes = [
    {
        path: '',
        component: GeolocationComponent,
    },
];
var GeolocationModule = /** @class */ (function () {
    function GeolocationModule() {
    }
    GeolocationModule = tslib_1.__decorate([
        NgModule({
            declarations: [GeolocationComponent],
            imports: [
                IonicModule,
                CommonModule,
                SharedModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            exports: [GeolocationComponent],
            providers: [
                SplashScreen,
                Geolocation,
            ]
        })
    ], GeolocationModule);
    return GeolocationModule;
}());
export { GeolocationModule };
//# sourceMappingURL=geolocation.module.js.map