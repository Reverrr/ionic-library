import * as tslib_1 from "tslib";
//Vendors
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavController } from '@ionic/angular';
var GeolocationComponent = /** @class */ (function () {
    function GeolocationComponent(navCtrl, 
    // private plt: Platform,
    geolocation) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.currentMapTrack = null;
        this.isTracking = false;
        this.trackedRoute = [];
        this.previousTracks = [];
    }
    // public ionViewDidLoad() {
    //     this.plt.ready().then(() => {
    //         this.loadHistoricRoutes();
    //         let mapOptions = {
    //             zoom: 13,
    //             mapTypeId: google.maps.MapTypeId.ROADMAP,
    //             mapTypeControl: false,
    //             streetViewControl: false,
    //             fullscreenControl: false
    //         }
    //         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //         this.geolocation.getCurrentPosition().then(pos => {
    //             let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
    //             this.map.setCenter(latLng);
    //             this.map.setZoom(16);
    //         }).catch((error) => {
    //             console.log('Error getting location', error);
    //         });
    //     });
    // }
    // public loadHistoricRoutes() {
    //     this.storage.get('routes').then(data => {
    //         if (data) {
    //             this.previousTracks = data;
    //         }
    //     });
    // }
    // public startTracking() {
    //     this.isTracking = true;
    //     this.trackedRoute = [];
    //     this.positionSubscription = this.geolocation.watchPosition()
    //       .pipe(
    //         filter((p) => p.coords !== undefined) //Filter Out Errors
    //       )
    //       .subscribe(data => {
    //         setTimeout(() => {
    //           this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
    //           this.redrawPath(this.trackedRoute);
    //         }, 0);
    //       });
    //   }
    // public redrawPath(path) {
    //     if (this.currentMapTrack) {
    //       this.currentMapTrack.setMap(null);
    //     }
    //     if (path.length > 1) {
    //       this.currentMapTrack = new google.maps.Polyline({
    //         path: path,
    //         geodesic: true,
    //         strokeColor: '#ff00ff',
    //         strokeOpacity: 1.0,
    //         strokeWeight: 3
    //       });
    //       this.currentMapTrack.setMap(this.map);
    //     }
    //   }
    // public stopTracking() {
    //     let newRoute = { finished: new Date().getTime(), path: this.trackedRoute };
    //     this.previousTracks.push(newRoute);
    //     this.storage.set('routes', this.previousTracks);
    //     this.isTracking = false;
    //     this.positionSubscription.unsubscribe();
    //     this.currentMapTrack.setMap(null);
    // }
    // public showHistoryRoute(route) {
    //     this.redrawPath(route);
    // }
    GeolocationComponent.prototype.getLocation = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            _this.currentLatitude = resp.coords.latitude;
            _this.currentLongitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            console.log(data);
            _this.latitude = data.coords.latitude;
            _this.longitude = data.coords.longitude;
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
        });
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", ElementRef)
    ], GeolocationComponent.prototype, "mapElement", void 0);
    GeolocationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-geolocation',
            templateUrl: 'geolocation.component.html',
            styleUrls: ['geolocation.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            Geolocation])
    ], GeolocationComponent);
    return GeolocationComponent;
}());
export { GeolocationComponent };
//# sourceMappingURL=geolocation.component.js.map