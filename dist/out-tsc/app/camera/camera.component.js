import * as tslib_1 from "tslib";
//Vendors
import { Component } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { ActionSheetController } from '@ionic/angular';
var CameraComponent = /** @class */ (function () {
    function CameraComponent(camera, actionSheetController) {
        this.camera = camera;
        this.actionSheetController = actionSheetController;
        this.myPhoto = "";
        this.images = [];
    }
    CameraComponent.prototype.makePhoto = function (typePhoto) {
        var _this = this;
        var options = {
            quality: 30,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: typePhoto,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true,
        };
        this.camera.getPicture(options).then(function (imageData) {
            var myPhoto = 'data:image/jpeg;base64,' + imageData;
            _this.images.push(myPhoto);
            _this.myPhoto = myPhoto;
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            // this.myPhoto = imageData;
        }, function (err) {
            _this.myPhoto = err;
            _this.images.push(err);
            console.log(err);
        });
    };
    CameraComponent.prototype.selectImage = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: "Select Image source",
                            buttons: [{
                                    text: 'Load from Library',
                                    handler: function () {
                                        _this.makePhoto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                    }
                                },
                                {
                                    text: 'Use Camera',
                                    handler: function () {
                                        _this.makePhoto(_this.camera.PictureSourceType.CAMERA);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel'
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CameraComponent = tslib_1.__decorate([
        Component({
            selector: 'app-camera',
            templateUrl: 'camera.component.html',
            styleUrls: ['camera.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Camera, ActionSheetController])
    ], CameraComponent);
    return CameraComponent;
}());
export { CameraComponent };
//# sourceMappingURL=camera.component.js.map