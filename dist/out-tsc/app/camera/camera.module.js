import * as tslib_1 from "tslib";
//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { CameraComponent } from './camera.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Camera } from '@ionic-native/camera/ngx';
var routes = [
    {
        path: '',
        component: CameraComponent,
    }
];
var CameraModule = /** @class */ (function () {
    function CameraModule() {
    }
    CameraModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                CameraComponent
            ],
            imports: [
                IonicModule,
                CommonModule,
                SharedModule,
                HttpClientModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            exports: [
                CameraComponent
            ],
            providers: [
                SplashScreen,
                Camera
            ]
        })
    ], CameraModule);
    return CameraModule;
}());
export { CameraModule };
//# sourceMappingURL=camera.module.js.map