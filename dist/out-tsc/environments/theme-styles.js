export var themeStylesConst = {
    normal: {
        main: '#3880ff',
        shade: '#3171e0',
        tint: '#4c8dff'
    },
    dark: {
        main: '#222428',
        shade: '#1e2023',
        tint: '#383a3e'
    },
    light: {
        main: '#989aa2',
        shade: '#86888f',
        tint: '#a2a4ab'
    },
    toxic: {
        main: '#10dc60',
        shade: '#0ec254',
        tint: '#28e070'
    }
};
//# sourceMappingURL=theme-styles.js.map