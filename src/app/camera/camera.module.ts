// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Components
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { CameraComponent } from './camera.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Camera } from '@ionic-native/camera/ngx';

const routes: Routes = [
    {
        path: '',
        component: CameraComponent,
    }
];

@NgModule({
    declarations: [
        CameraComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        CameraComponent
    ],
    providers: [
        SplashScreen,
        Camera
    ]
})

export class CameraModule { }
