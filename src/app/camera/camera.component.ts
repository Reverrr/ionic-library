// Vendors
import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ActionSheetController } from '@ionic/angular';
import { UserModel, UserAccessModel } from '@app/shared/models';
import { UserService } from '@app/shared/services/user.service';
import { AuthenticationService } from '@app/shared/services';

@Component({
    selector: 'app-camera',
    templateUrl: 'camera.component.html',
    styleUrls: ['camera.component.scss']
})
export class CameraComponent {
    public user: UserModel = new UserModel();

    public images: string[] = [];

    constructor(
        private camera: Camera,
        private actionSheetController: ActionSheetController,
        public userService: UserService,
        public auth: AuthenticationService,
    ) {
        this.getPhotos();
    }

    public myPhoto = ``;

    public getPhotos() {
        this.auth.checkCurrentUser().subscribe((data: UserAccessModel) => {
            this.user = data.userModel;
            this.images = data.userModel.images;
        });
    }

    public makePhoto(typePhoto) {
        const options: CameraOptions = {
            quality: 30,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: typePhoto,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true,
        };

        this.camera.getPicture(options).then((imageData) => {
            const myPhoto = 'data:image/jpeg;base64,' + imageData;
            this.images.push(myPhoto);
            this.user.images = this.images;
            this.userService.updateUserImages(this.user).subscribe();
        }, (err) => {
            this.myPhoto = err;
            this.images.push(err);
            this.userService.updateUserImages(this.user).subscribe();

        });
    }

    public async selectImage() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Select Image source',
            buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.makePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.makePhoto(this.camera.PictureSourceType.CAMERA);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
            ]
        });
        await actionSheet.present();
    }

    public loadImages(event) {
        this.auth.checkCurrentUser().subscribe((data: UserAccessModel) => {
            this.user = data.userModel;
            this.images = data.userModel.images;
            event.target.complete();
            event.target.disabled = false;
        });
    }

    public removeImage(index: number) {
        this.user.images.splice(index, 1);
        this.userService.updateUserImages(this.user).subscribe();
    }
}
