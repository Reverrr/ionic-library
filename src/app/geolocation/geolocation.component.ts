//Vendors
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { googleMapsClient } from '@google/maps';
import { Subscription } from 'rxjs';
import { NavController, Platform } from '@ionic/angular';
import { filter } from 'rxjs/operators';

declare const google;

@Component({
    selector: 'app-geolocation',
    templateUrl: 'geolocation.component.html',
    styleUrls: ['geolocation.component.scss']
})
export class GeolocationComponent {
    @ViewChild('map', null) mapElement: ElementRef;
    public map: any;
    public currentMapTrack = null;

    public isTracking: boolean = false;
    public positionSubscription: Subscription;
    public trackedRoute = [];
    public previousTracks = [];

    public latitude;
    public longitude;
    public currentLatitude;
    public currentLongitude;
    constructor(
        public navCtrl: NavController,
        private geolocation: Geolocation,
    ) { }


    public getLocation() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.currentLatitude = resp.coords.latitude;
            this.currentLongitude = resp.coords.longitude;

        }).catch((error) => {
            console.log('Error getting location', error);
        });

        const watch = this.geolocation.watchPosition();
        watch.subscribe((data) => {
            this.latitude = data.coords.latitude;
            this.longitude = data.coords.longitude;
        });
    }


}
