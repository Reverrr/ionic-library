//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { GeolocationComponent } from './geolocation.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

const routes: Routes = [
    {
        path: '',
        component: GeolocationComponent,
    },
];

@NgModule({
    declarations: [GeolocationComponent],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [GeolocationComponent],
    providers: [
        SplashScreen,
        Geolocation,
        
    ]
})

export class GeolocationModule { }
