import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '@app/shared/guards/auth.guard';
import { RoleEnum } from '@app/shared/enums/role.enum';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'library',
        loadChildren: () => import('../library/library.module').then(m => m.LibraryModule)
      },
      {
        path: 'admin-pages',
        loadChildren: () => import('../admin-pages/admin-pages.module').then(m => m.AdminPagesModule),
        canActivate: [AuthGuard], data: {role: [
          RoleEnum.admin
      ]}
      },
      {
        path: 'chat',
        loadChildren: () => import('../chat/chat.module').then(m => m.ChatModule)
      },
      {
        path: 'users',
        loadChildren: () => import('../users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'camera',
        loadChildren: () => import('../camera/camera.module').then(m => m.CameraModule)
      },
      {
        path: 'geolocation',
        loadChildren: () => import('../geolocation/geolocation.module').then(m => m.GeolocationModule)
      },
      {
        path: 'gallery',
        loadChildren: () => import('../gallery-images/gallery-images.module').then(m => m.GalleryImagesModule)
      },
      {
        path: 'draw-ground',
        loadChildren: () => import('../draw-graund/draw-ground.module').then(m => m.DrawGroundModule)
      },
      {
        path: '',
        redirectTo: '/tabs/library',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/library',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
