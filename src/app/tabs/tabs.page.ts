import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/shared/services';
import { ChatService } from '@app/shared/services/chat.service';
import { MenuService } from '@app/shared/services/menu.service';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '@app/shared/components/popover/popover.component';
import { UserAccessModel } from '@app/shared/models/user-access.model';
import { RoleEnum } from '@app/shared/enums';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  public unreadMessages = 0;
  public animationsUnreadMessages = false;
  public isAdmin: boolean;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private chatService: ChatService,
    private menuService: MenuService,
    public popoverController: PopoverController
  ) {
    this.chatService.getMessage().subscribe((message) => {
      if (this.router.url !== '/tabs/chat') {
        this.unreadMessages++;
        this.animationsUnreadMessages = false;
        setTimeout(() => {
            this.animationsUnreadMessages = true;
        }, 100);
      } else {
        this.unreadMessages = 0;
      }
    });
    this.authenticationService.checkCurrentUser().subscribe((user: UserAccessModel) => {
     if (user.userModel.role === RoleEnum.admin) {
        this.isAdmin = true;
     }
    });
  }

  public async presentPopover(ev) {
    const popover = await this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  public clearUnreadMessage() {
    this.unreadMessages = 0;
  }

  public closeMenu() {
    this.menuService.closeMenuEvent();
  }

  public logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['auth/login']);
  }

}
