// Vendors
import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    AfterViewChecked,
    OnDestroy
} from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
// Services
import { ChatService } from '@app/shared/services/chat.service';
import { AuthenticationService } from '@app/shared/services';
// Model
import { ChatModel } from '@app/shared/models/chat.model';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
    selector: 'app-main-chat',
    templateUrl: './main-chat.component.html',
    styleUrls: ['./main-chat.component.scss']
})
export class MainChatComponent implements OnInit, OnDestroy, AfterViewChecked {
    public message = '';
    public toggleMessageBool = false;
    public messages: ChatModel[] = [];
    public onlineUsers: number;
    public curentUserSubscribe: Subscription;
    public getMessageSubscribe: Subscription;
    public subscribeOnlineUsers: Subscription;
    public unreadMessages = 0;
    public animationsUnreadMessages = false;
    public editFormGroup: FormGroup = new FormGroup({
        message: new FormControl('', [Validators.required, Validators.min(1)])
    });
    constructor(
        private socket: ChatService,
        private authService: AuthenticationService,
        private localNotifications: LocalNotifications
    ) { }

    @ViewChild('scroll', null) private myScrollContainer: ElementRef;

    ngOnInit() {
        this.socket.getChatStatus().subscribe((status: ChatModel) => {
            this.toggleMessageBool = status.statusWindow;
            this.onlineUsers = status.online;
        });
        this.getMessageSubscribe = this.socket
            .getMessage()
            .subscribe((message: ChatModel) => {
                this.curentUserSubscribe = this.authService
                    .getCurrentUser()
                    .subscribe(data => {
                        this.localNotifications.schedule({
                            title: `Message`,
                            text: `${data.username}: ${message.message}`,
                            led: '000000',
                            sound: null
                        });
                        if (!!this.curentUserSubscribe) {
                            this.curentUserSubscribe.unsubscribe();
                        }
                        if (data && message.username === data.username) {
                            message.color = true;
                        }
                        this.messages.push(message);
                        this.scrollToBottom();
                        if (!this.toggleMessageBool) {
                            this.unreadMessages++;
                            this.animationsUnreadMessages = false;
                            setTimeout(() => {
                                this.animationsUnreadMessages = true;
                            }, 100);
                        } else {
                            this.unreadMessages = 0;
                        }
                    });
            });
        this.subscribeOnlineUsers = this.socket
            .getTotalOnlineUsers()
            .subscribe((data: number) => {
                this.onlineUsers = data;
                this.socket.setChatOptions(this.toggleMessageBool, data);
            });
        this.socket.getMessageArray().subscribe((data: ChatModel[]) => {
            this.messages = data;
        });
    }

    doRefresh(event) {
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 2000);
    }

    ngOnDestroy() {
        this.getMessageSubscribe.unsubscribe();
        this.subscribeOnlineUsers.unsubscribe();
        this.socket.setMessageArray(this.messages);
    }

    ngAfterViewChecked() {
        this.scrollToBottom();
    }

    private scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch (err) { }
    }

    public sendMessage(event: Event) {
        if (this.editFormGroup.valid) {
            this.socket.sendMessage(this.editFormGroup.controls.message.value);
            this.editFormGroup.reset();
        }
    }

    public toggleMessage(): void {
        this.toggleMessageBool = !this.toggleMessageBool;
        this.unreadMessages = 0;
        this.socket.setChatOptions(this.toggleMessageBool, this.onlineUsers);
    }
}
