// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Components
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { MainChatComponent } from '@app/chat/main-chat/main-chat.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { environment } from '../../environments/environment';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
// Routing

const config: SocketIoConfig = { url: environment.apiUrl, options: {} };
// const config: SocketIoConfig = { url: 'http://localhost:6768', options: {} };

@NgModule({
  declarations: [
    MainChatComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IonicModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config),
    RouterModule.forChild([{ path: '', component: MainChatComponent }]),
  ],
  exports: [
    MainChatComponent
  ],
  providers: [
    LocalNotifications
  ]
})

export class ChatModule {}
