import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/shared/guards/auth.guard';
import { RoleEnum } from '@app/shared/enums';

const routes: Routes = [
  {
    path: 'auth',
    canActivate: [AuthGuard], data: { role: [] },
    loadChildren: './auth/authorization.module#AuthorizationModule'
  },
  {
    path: '',
    canActivate: [AuthGuard], data: {role: [RoleEnum.admin, RoleEnum.user]},
    loadChildren: './tabs/tabs.module#TabsPageModule'
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
