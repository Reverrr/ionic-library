// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Components
import { LoginComponent } from '@app/auth/login';
import { RegisterComponent } from '@app/auth/register';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

const routes: Routes = [
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: '',
        redirectTo: '/auth/register',
        pathMatch: 'full'
    }
];

@NgModule({
    declarations: [
        RegisterComponent,
        LoginComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
        RegisterComponent,
    ],
    providers: [
        AndroidFingerprintAuth,
        File,
    ]
})

export class AuthorizationModule { }
