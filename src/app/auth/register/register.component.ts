﻿// Vendors
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators, AbstractControl, FormControl} from '@angular/forms';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';
// Services
import { UserService } from '@app/shared/services';
// Models
import { UserModel, KeyValueModel } from '@app/shared/models';
import { Subscription } from 'rxjs';
import {ToastController} from '@ionic/angular';
import {FormValidatorHelper} from '@app/shared/helpers/form-validator.helper';

@Component({
    selector: 'app-register',
    templateUrl: 'register.component.html'
})
export class RegisterComponent {

    public registerForm: FormGroup = new FormGroup({
        email: new FormControl('', [Validators.required, FormValidatorHelper.email]),
        username: new FormControl('', [Validators.required, FormValidatorHelper.username]),
        password: new FormControl('', [Validators.required, FormValidatorHelper.password])
    });

    private user: UserModel;
    private subscribe: Subscription;
    public errMessage: string;

    public errorMessage;
    public successMessage;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private location: Location,
        private userService: UserService,
        private toastController: ToastController
    ) { }

    public get f(): KeyValueModel<AbstractControl> {
        return this.registerForm.controls;
    }

    public tryRegister() {
        this.registerForm.markAllAsTouched();

        console.log(this.registerForm);
        if (this.registerForm.controls['email'].errors) {
            this.runToast('Incorrect email');
            return;
        }
        if (this.registerForm.controls['username'].errors) {
            this.runToast('Incorrect username');
            return;
        }
        if (this.registerForm.controls['password'].errors) {
            this.runToast('Incorrect password');
            return;
        }

        if (this.registerForm.invalid) {
            return;
        }
        this.subscribe = this.userService.register(this.registerForm.value).pipe(first()).subscribe(
            (data: UserModel) => {
                this.location.back();
                this.subscribe.unsubscribe();
            },
            (error) => {
                console.log(error);
                this.errMessage = error;
            });
    }

    public async runToast(message: string) {
        const toast = await this.toastController.create({
            message,
            duration: 2000
        });
        toast.present();
    }
}
