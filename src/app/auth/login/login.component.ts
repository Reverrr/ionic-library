﻿// Vendors
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
// Services
import { AuthenticationService } from '@app/shared/services';
// Models
import { UserAccessModel, KeyValueModel, ErrorModel } from '@app/shared/models';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { FingerprintService } from '@app/shared/services/fingerprint.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent
    implements OnInit {
    public loginForm: FormGroup;
    public loading = false;
    public submitted = false;
    public returnUrl: string;
    public failToGetData: boolean;
    public isTokenForFinger: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private androidFingerprintAuth: AndroidFingerprintAuth,
        private fingerprintService: FingerprintService,
    ) {
        if (localStorage.getItem('currentUser')) {
            this.router.navigate(['/']);
        }
        try {
            if (localStorage.getItem('tokenForFinger')) {
                this.isTokenForFinger = true;
            }
        } catch (err) { }
    }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    public redirectToReg(): void {
        this.router.navigateByUrl('/register');
    }

    get f(): KeyValueModel<AbstractControl> {
        return this.loginForm.controls;
    }

    public onSubmit(): void {
        /**
         * Temp for Android
         */
        // this.router.navigate([this.returnUrl]);
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        this.authenticationService.login(this.loginForm.value.username, this.loginForm.value.password)
        .pipe(first())
        .subscribe((data: UserAccessModel) => {
                    if (data) {
                        this.failToGetData = false;
                        this.fingerprintService.setFingerPrint(this.loginForm.value.username, this.loginForm.value.password);
                        this.router.navigate([this.returnUrl]);
                        this.loading = false;
                    } else {
                        this.failToGetData = true;
                        this.loading = false;
                    }
                },
                (error: ErrorModel<string, number>) => {
                    this.router.navigate(['/']);
                    this.loading = false;
                }
            );
    }

    public async checkExist() {
        const token = localStorage.getItem('tokenForFinger');
        const result = await this.androidFingerprintAuth.decrypt({ clientId: 'currentUser', token: token, disableBackup: true });
        const { username, password } = JSON.parse(result.password);
        if (result) {
            this.authenticationService.login(username, password)
            .subscribe((data: UserAccessModel) => {
                    if (data) {
                        this.failToGetData = false;
                        this.router.navigate([this.returnUrl]);
                        this.loading = false;
                    } else {
                        this.failToGetData = true;
                        this.loading = false;
                    }
                },
                (error: ErrorModel<string, number>) => {
                    this.router.navigate(['/registration']);
                    this.loading = false;
                }
            );
        } else if (result.withBackup) {
        } else {
          console.log('Didn\'t authenticate!');
        }
      }

}
