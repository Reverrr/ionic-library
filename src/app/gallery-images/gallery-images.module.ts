// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Components
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GalleryImagesComponent } from './gallery-images.component';

import { Camera } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

import { IonicStorageModule } from '@ionic/storage';

const routes: Routes = [
    {
        path: '',
        component: GalleryImagesComponent,
    },
];

@NgModule({
    declarations: [GalleryImagesComponent],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        IonicStorageModule.forRoot(),
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [GalleryImagesComponent],
    providers: [
        SplashScreen,
        Geolocation,
        Camera,
        File,
        WebView,
        FilePath
    ]
})

export class GalleryImagesModule { }
