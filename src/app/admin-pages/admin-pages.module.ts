// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Components
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { AdminComponent } from './admin-layout/admin.component';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { SettingLibraryComponent } from './setting-library/setting-library.component';
import { SettingBookComponent } from './setting-book/setting-book.component';
import { SettingAuthorsComponent } from './setting-authors/setting-authors.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'edit-users' },
            { path: 'edit-users', component: EditUsersComponent },
            { path: 'edit-users/edit-user/:id', component: EditUserComponent },
            { path: 'setting', component: SettingLibraryComponent },
            { path: 'edit-authors', component: SettingAuthorsComponent },
            { path: 'setting/add-new-book', component: SettingBookComponent, pathMatch: 'full' },
            { path: 'setting/:book', component: SettingBookComponent },
            { path: '**', redirectTo: 'edit-users' }
        ]
    }
];

@NgModule({
    declarations: [
        AdminComponent,
        EditUsersComponent,
        SettingLibraryComponent,
        EditUserComponent,
        SettingBookComponent,
        SettingAuthorsComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        NgxSmartModalModule.forChild(),
        RouterModule.forChild(routes)
    ],
    exports: [
        AdminComponent,
    ]
})

export class AdminPagesModule { }
