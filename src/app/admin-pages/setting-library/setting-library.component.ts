// Vendors
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
// Services
import { LibraryService } from '@app/shared/services/library.service';
// Model
import { BookModel, CountModel, ErrorModel } from '@app/shared/models';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';

@Component({
    selector: 'setting-library',
    templateUrl: './setting-library.component.html',
    styleUrls: ['./setting-library.component.scss']
})

export class SettingLibraryComponent implements OnInit {

    public books: BookModel[];
    public countTotalBooks: number;
    private currentPage = 1;

    @ViewChild(IonInfiniteScroll, null) infiniteScroll: IonInfiniteScroll;

    constructor(
        public libraryService: LibraryService,
        private router: Router,
        public ngxSmartModalService: NgxSmartModalService,
        public alertController: AlertController,
    ) {
        this.libraryService.getBooksForPagination(1, 10).subscribe(
            (data: BookModel[]) => {
                this.books = data;
            },
            (err: ErrorModel<string, number>) => { throw err; }
        );
    }

    ngOnInit() {}

    public selectedBook(id: string): void {
        this.router.navigateByUrl(`tabs/admin-pages/setting/${id}`);
    }

    public changeCurrentPage(currentPage: number): void {
        this.currentPage = currentPage;
        this.libraryService.getBooksForPagination(currentPage, 10).subscribe(
            (data: BookModel[]) => { this.books = data; },
            (err: ErrorModel<string, number>) => { throw err; }
        );
    }

    public async presentAlert(id: string, title: string, index: number) {
        const alert = await this.alertController.create({
            header: 'Confirm!',
            message: `Delete book: ${title}`,
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: (blah) => {
                }
              }, {
                text: 'Delete',
                cssClass: 'btn-delete',
                handler: (data) => {
                    this.deleteBook(id, index);
                }
              }
            ]
          });

          await alert.present();
      }

    public loadData(event): void {
        this.currentPage++;
        this.libraryService.getBooksForPagination(this.currentPage, 10).subscribe(
            (data: BookModel[]) => {
                event.target.complete();
                event.target.disabled = false;
                this.books.push(...data);
            },
            (err: ErrorModel<string, number>) => { throw err; }
        );
    }

    public deleteBook(id: string, index: number): void {
        this.countTotalBooks -= 1;
        this.libraryService.deleteBook(id).subscribe(
            (data: BookModel) => {
                this.books.splice(index, 1);
            },
            (err: ErrorModel<string, number>) => { throw err; }
        );
    }

    public selectedId(id: string): void {
        this.ngxSmartModalService.resetModalData('deleteLibraryPopap');
        this.ngxSmartModalService.setModalData(id, 'deleteLibraryPopap');
    }
}
