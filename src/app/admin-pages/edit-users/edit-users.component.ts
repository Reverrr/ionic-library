﻿// Vendors
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { NgxSmartModalService } from 'ngx-smart-modal';
// Models
import { UserModel, ErrorModel, CountModel } from '@app/shared/models';
// Services
import { UserService, AuthenticationService } from '@app/shared/services';
import { AlertController, IonInfiniteScroll } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
    templateUrl: 'edit-users.component.html',
    styleUrls: ['./edit-users.component.scss']
})
export class EditUsersComponent implements OnInit, OnDestroy {
    public currentUser: UserModel;
    public currentUserSubscription: Subscription;
    public countTotalUsers: number;
    public users: UserModel[] = [];
    private currentPage = 1;
    public ionPageLoad = 0;

    @ViewChild(IonInfiniteScroll, null) infiniteScroll: IonInfiniteScroll;

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private router: Router,
        public ngxSmartModalService: NgxSmartModalService,
        public alertController: AlertController
    ) {
        this.currentUserSubscription = this.authenticationService.getCurrentUser().subscribe((user: UserModel) => {
            this.currentUser = user;
         });
    }

    public ngOnInit(): void {
        this.loadUsersForPagination(this.currentPage);
    }

    public async presentAlert(id: string, username: string) {
        const alert = await this.alertController.create({
            header: 'Confirm!',
            message: `Delete user: ${username}`,
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: (blah) => {
                }
              }, {
                text: 'Delete',
                cssClass: 'btn-delete',
                handler: (data) => {
                    this.deleteUser(id);
                }
              }
            ]
          });

          await alert.present();
      }

    public ngOnDestroy(): void {
        this.currentUserSubscription.unsubscribe();
    }

    public editUser(index: number): void {
        this.router.navigate(['tabs/admin-pages/edit-users/edit-user', this.users[index].id]);
    }

    public deleteUser(id: string): void {
            this.countTotalUsers -= 1;
            if (!this.countTotalUsers && this.currentPage !== 1) { this.currentPage -= 1; }
            this.userService.delete(id).pipe(first()).subscribe(() => {
                this.countTotalUsers -= 1;
                this.loadUsersForPagination(this.currentPage);
            });
    }

    private loadUsersForPagination(currentPage: number): void {
        this.currentPage++;
        this.userService.getUsersForPagination(currentPage, 20).subscribe(
            (data: UserModel[]) => {
                this.users = data;
            },
            (err: ErrorModel<string, number>) => {throw err; }
        );
    }
    public loadData(event) {
        this.currentPage++;
        this.userService.getUsersForPagination(this.currentPage, 20).subscribe(
            (data: UserModel[]) => {
                event.target.complete();
                event.target.disabled = false;
                this.users.push(...data);
             },
            (err: ErrorModel<string, number>) => {throw err; }
        );
    }

    public toggleInfiniteScroll(): void {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }

    public changeCurrentPage(currentPage: number): void {
        this.currentPage = currentPage;
        this.userService.getUsersForPagination(currentPage, 20).subscribe(
            (data: UserModel[]) => { this.users = data; },
            (err: ErrorModel<string, number>) => {throw err; }
        );
    }

    public selectedId(id: string): void {
        this.ngxSmartModalService.resetModalData('deleteUserPopap');
        this.ngxSmartModalService.setModalData(id, 'deleteUserPopap');
    }

}
