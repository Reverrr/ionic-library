import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { IWriteOptions } from '@ionic-native/file/ngx';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-draw-ground',
  templateUrl: 'draw-ground.component.html',
  styleUrls: ['draw-ground.component.scss']
})
export class DrawGroundComponent implements OnInit {
  @ViewChild('canvas', { static: true }) canvasEl: ElementRef;
  storedImages: any[] = [];

  private fingersDown: number = 0;

  private canvas: any;
  private context: any;
  public saveX;
  public saveY;

  finger1 = {
    saveX: 0,
    saveY: 0
  };
  finger2 = {
    saveX: 0,
    saveY: 0
  };
  constructor(
    public file: File,
    private platform: Platform
  ) {}

  ngOnInit() {
    console.log(window.innerWidth);
    
    this.canvas = this.canvasEl.nativeElement;
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerWidth;
    this.initialiseCanvas();
    this.drawCircle();
  }

  ionViewDidLoad() {}
  initialiseCanvas() {
    if (this.canvas.getContext) {
      this.setupCanvas();
    }
  }
  setupCanvas() {
    this.context = this.canvas.getContext('2d');
    this.context.fillStyle = '#3e3e3e';
    this.context.fillRect(0, 0, window.innerWidth, window.innerWidth);
  }
  clearCanvas() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.setupCanvas();
  }
  drawCircle() {
    this.clearCanvas();
    this.context.beginPath();

    this.context.arc(this.canvas.width / 2, this.canvas.height / 2, 80, 0, 2 * Math.PI);
    this.context.lineWidth = 1;
    this.context.strokeStyle = '#ffffff';
    this.context.stroke();
  }

  startDrawing(ev) {
    console.log('startDrawing');

    
    this.fingersDown += 1;
    console.log(this.fingersDown + ' ' + 'fingers');
    
    ev.preventDefault();
    ev.stopPropagation();
    const canvasPosition = this.canvas.getBoundingClientRect();
    this.finger1.saveX = ev.touches[0].pageX - canvasPosition.x;
    this.finger1.saveY = ev.touches[0].pageY - canvasPosition.y;
  }

  drawSquare(): void {
    this.clearCanvas();
    this.context.beginPath();
    this.context.rect(
      this.canvas.width / 2 - 100,
      this.canvas.height / 2 - 100,
      200,
      200
    );
    this.context.lineWidth = 1;
    this.context.strokeStyle = '#ffffff';
    this.context.stroke();
  }

  moved(ev) {
    // console.log('moved');
    const canvasPosition = this.canvas.getBoundingClientRect();
    const touches = ev.touches;
    console.log(touches);
    
    const points = []
    for (let i = 0; i < touches.length; i++) {
      const changedTouch = touches[i];
      const key = changedTouch.identifier;
      const mapboxTouch = ev.lngLats[i];
      const item = {
        ...mapboxTouch,
        id: key,
      }
      points.push(item)
    }
    console.log(points);
    const ctx = this.canvas.getContext('2d');
    const currentX = ev.touches[0].pageX - canvasPosition.x;
    const currentY = ev.touches[0].pageY - canvasPosition.y;
  //  console.log(currentX);

    ctx.lineJoin = 'round';
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 5;

    ctx.beginPath();
    ctx.moveTo(this.finger1.saveX, this.finger1.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();

    ctx.stroke();

    this.finger1.saveX = currentX;
    this.finger1.saveY = currentY;
  }

  untouched(event) {
    this.fingersDown -= 1;
    this.finger1.saveX = null;
    this.finger1.saveY = null;
  }

  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    const sliceSize = 512;
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
   
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
   
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
   
      const byteArray = new Uint8Array(byteNumbers);
   
      byteArrays.push(byteArray);
    }
   
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  saveCanvasImage() {
    const dataUrl = this.canvas.toDataURL();
    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
    const name = new Date().getTime() + '.png';
    const path = this.file.dataDirectory;
    const options: IWriteOptions = { replace: true };
    const data = dataUrl.split(',')[1];
    const blob = this.b64toBlob(data, 'image/png');
    this.storeImage(name);
  }

  storeImage(imageName) {
    const saveObj = { img: imageName };
    this.storedImages.push(saveObj);
    // this.storage.set('', this.storedImages).then(() => {
    //   setTimeout(() =>  {
    //     this.content.scrollToBottom();
    //   }, 500);
    // });
  }

}
