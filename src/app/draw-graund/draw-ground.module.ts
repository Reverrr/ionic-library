import { NgModule } from '@angular/core';
import { DrawGroundComponent } from './draw-ground.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: DrawGroundComponent,
    }
];

@NgModule({
    declarations: [
        DrawGroundComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ]
})

export class DrawGroundModule {

}
