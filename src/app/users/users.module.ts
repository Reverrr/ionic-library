//Vendors
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@app/shared/shared.module';
import { UsersComponent } from './users.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
    },
    {
        path: ':id',
        component: UserComponent,
    },
];

@NgModule({
    declarations: [
        UsersComponent,
        UserComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        UsersComponent,
        UserComponent
    ]
})

export class UsersModule { }
