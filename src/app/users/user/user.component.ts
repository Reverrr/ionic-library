// Vendors
import { Component } from '@angular/core';
import { UserModel } from '@app/shared/models';
import { UserService } from '@app/shared/services';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-user',
    templateUrl: 'user.component.html',
    styleUrls: ['user.component.scss']
})
export class UserComponent {
  public user: UserModel;

  constructor(
    public userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    ) {
        this.userService.getById(this.route.snapshot.params.id).subscribe((user: UserModel) => {
            this.user = user;
        });
    }


}
