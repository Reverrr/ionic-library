// Vendors
import { Component } from '@angular/core';
import { UserModel } from '@app/shared/models';
import { UserService } from '@app/shared/services';
import { Router } from '@angular/router';


@Component({
    selector: 'app-users',
    templateUrl: 'users.component.html',
    styleUrls: ['users.component.scss']
})
export class UsersComponent {
  public users: UserModel[] = [];

  constructor(
    public userService: UserService,
    private router: Router
    ) {
        this.userService.getAll().subscribe((users) => {
            this.users = users;
        });
    }

    public redirectToUser(id: string) {
        this.router.navigateByUrl(`tabs/users/${id}`);
    }

}
