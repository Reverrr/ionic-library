// Vendors
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
// Services
import { LibraryService } from '@app/shared/services/library.service';
// Environment
import { environment } from '@environments/environment';
import { BookTypeEnum } from '@app/shared/enums/book-type.enum';
// Models
import { BookModel, ErrorModel } from '@app/shared/models';
import { IonInfiniteScroll, IonContent, AlertController, PopoverController } from '@ionic/angular';

@Component({
    selector: 'app-libary',
    templateUrl: './library.component.html',
    styleUrls: ['./library.component.scss']
})

export class LibraryComponent implements OnInit {
    public filterArrayAuthors: string[] = [];
    public selectedTypeBook = '';
    public books: BookModel[];
    private currentPage = 1;
    public totalCountBooks: number;
    private searchingStatus: boolean;
    public valueSearch = '';
    public type = BookTypeEnum;
    public toggled = false;
    public scrollToTop: boolean;
    public showSearch = false;
    public isFinishedLoadData: boolean;

    @ViewChild(IonInfiniteScroll, null) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonContent, null) content: IonContent;

    constructor(
        public libraryService: LibraryService,
        public router: Router,
        public alertController: AlertController,
        public popoverController: PopoverController,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        this.toggled = false;
        this.loadAllBooks();
        setInterval(() => {
            // Let's refresh the list.
            this.changeDetectorRef.markForCheck();
          }, 1000 * 5);
    }
    ngOnInit() { }


    public async presentAlert() {
        const alert = await this.alertController.create({
            header: 'Select Type Book',
            inputs: [
                {
                    name: BookTypeEnum.all,
                    type: 'radio',
                    label: BookTypeEnum.all,
                    value: ''
                },
                {
                    name: BookTypeEnum.book,
                    type: 'radio',
                    label: BookTypeEnum.book,
                    value: BookTypeEnum.book
                },

                {
                    name: BookTypeEnum.magazine,
                    type: 'radio',
                    label: BookTypeEnum.magazine,
                    value: BookTypeEnum.magazine
                },

            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => { }
                }, {
                    text: 'Ok',
                    handler: (event: string) => {
                        this.selectedTypeBook = event;
                        this.getSearchBooks(this.valueSearch);
                    }
                }
            ]
        });
        await alert.present();
    }

    public toogleSearch(): void {
        this.showSearch = !this.showSearch;
    }

    public toggle(): void {
        this.toggled = !this.toggled;
    }

    public cancelSearch(event: Event): void {
        this.toggled = false;
    }

    public logScrollStart(): void {
        this.content.scrollToTop(1500).then((data) => {
            this.scrollToTop = false;
        });
    }

    public loadData(event): void {
        this.currentPage++;
        this.scrollToTop = true;
        this.libraryService.searchBooks(
            this.valueSearch, 6,
            this.filterArrayAuthors,
            (this.selectedTypeBook === this.type.all) ?
                '' :
                this.selectedTypeBook,
                this.currentPage).subscribe(
                    (data: BookModel[]) => {
                        console.log(data);
                        event.target.complete();
                        if (!data.length) {
                            this.isFinishedLoadData = true;
                        }
                        event.target.complete();
                        event.target.disabled = false;
                        this.books.push(...data);
                    },
                    (err: ErrorModel<string, number>) => { throw err; }
                );
    }

    public toggleInfiniteScroll(): void {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }

    public searchEvent(event: Event): void {
        const e = event.target as HTMLInputElement;
        this.valueSearch = e.value;
        this.getSearchBooks(this.valueSearch);
    }

    public changeCurrentPage(page: number): void {
        this.currentPage = page;
        this.libraryService.searchBooks(
            this.valueSearch,
            6,
            this.filterArrayAuthors,
            (this.selectedTypeBook === this.type.all) ?
                '' :
                this.selectedTypeBook,
            this.currentPage).subscribe(
                (data: BookModel[]) => {
                    this.books = data;
                },
                (err: ErrorModel<string, number>) => { throw err; }
            );
    }

    public onChanged(authors: string[]): void {
        this.filterArrayAuthors = authors;
        this.getSearchBooks(this.valueSearch);
    }

    public checkTypeSelected(event: Event): void {
        const e = event.target as HTMLInputElement;
        this.selectedTypeBook = e.value;
        this.getSearchBooks(this.valueSearch);
    }

    private getSearchBooks(valueSearch: string): void {
        if (!valueSearch &&
            !this.filterArrayAuthors.length &&
            !this.selectedTypeBook ||
            this.selectedTypeBook === 'all' &&
            !valueSearch &&
            !this.filterArrayAuthors.length) {
            this.searchingStatus = false;
        } else {
            this.searchingStatus = true;
            this.currentPage = 1;
        }
        this.valueSearch = valueSearch;
        this.libraryService.searchBooks(valueSearch,
            6,
            this.filterArrayAuthors,
            (this.selectedTypeBook === this.type.all) ?
            '' :
            this.selectedTypeBook,
            this.currentPage).subscribe(
                (data: BookModel[]) => {
                    this.books = data;
                },
                (err: ErrorModel<string, number>) => { throw err; }
            );
    }

    public updateUrl(book: BookModel): void {
        book.img = environment.defaultBookImgUrl;
    }

    private loadAllBooks(): void {
        this.libraryService.searchBooks(
            this.valueSearch,
            6,
            this.filterArrayAuthors,
            (this.selectedTypeBook === this.type.all) ?
            '' :
            this.selectedTypeBook,
            this.currentPage
            ).subscribe(
                (data: BookModel[]) => {
                    this.books = data;
                },
                (err: ErrorModel<string, number>) => { throw err; }
            );
    }

    public selectBook(index: number): void {
        this.libraryService.dispatch(this.books[index]);
        this.router.navigateByUrl(`/tabs/library/${this.books[index].id}`);
    }
}
