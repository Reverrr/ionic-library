import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LibraryComponent } from '@app/library/library/library.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '@app/shared/shared.module';
import { BookComponent } from '@app/library/book/book.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
        RouterModule.forChild([
            {
                path: '', component: LibraryComponent
            },
            {
                path: ':id',
                component: BookComponent
            }
        ])
  ],
  declarations: [
      LibraryComponent,
      BookComponent
    ]
})
export class LibraryModule { }
