// Vendors
import { Component } from '@angular/core';
import { UserModel, UserAccessModel, ErrorModel } from '@app/shared/models';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserService, AuthenticationService } from '@app/shared/services';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { themeStylesConst } from '@environments/theme-styles';
import { themeStylesEnum } from '@app/shared/enums/theme-styles-enum';
import { MenuService } from '../shared/services/menu.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { FingerprintService } from '../shared/services/fingerprint.service';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss']
})
export class ProfileComponent {
  public user: UserModel = new UserModel();
  public currentUser: UserModel;
  public isCurrentUserAdmin: boolean;
  public errorMessage: string;
  public enum = themeStylesEnum;

  public showFingerSignIn;
  public showSettingUser;
  public selectThemes;
  public isFingerTokenExist;

  public editFormGroup: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18), Validators.pattern('^[[A-Za-z0-9]+]*$')]),
    email: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(18), Validators.pattern('^[a-zA-Z]*$')]),
  });
  public registerFingerForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(2)]),
    password: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(18)]),
  });


  constructor(
    public userService: UserService,
    private router: Router,
    private location: Location,
    public auth: AuthenticationService,
    public modalController: ModalController,
    private menuService: MenuService,
    private localNotifications: LocalNotifications,
    private fingerprintService: FingerprintService
  ) {
    try {
      localStorage.getItem('tokenForFinger');
      this.isFingerTokenExist = true;
    } catch (err) { }
    this.auth.checkCurrentUser().subscribe((data: UserAccessModel) => {
      this.currentUser = data.userModel;
      this.editFormGroup.setValue({
        username: this.currentUser.username,
        email:  (this.currentUser.email) ? this.currentUser.email : '',
      });
      this.user = data.userModel;
    });
  }

  public async enableNotafication() {
    this.localNotifications.schedule({
      text: 'Remember about me...',
      led: 'ffffff',
      sound: null
    });
  }

  OnInit() { }

  public async openModal(): Promise<void> {
    this.router.navigateByUrl('/tabs/profile/');
  }

  private checkCurrentUser(): void {
    this.auth.checkCurrentUser().subscribe(
      (data: UserAccessModel) => {
        this.currentUser = data.userModel;
      }
    );
  }

  public selectedTheme(event): void {
    if (event.detail.value === this.enum.light) {
      this.userService.setColorTheme(themeStylesConst.light);
    } else if (event.detail.value === this.enum.dark) {
      this.userService.setColorTheme(themeStylesConst.dark);
    } else if (event.detail.value === this.enum.toxic) {
      this.userService.setColorTheme(themeStylesConst.toxic);
    } else if (event.detail.value === this.enum.normal) {
      this.userService.setColorTheme(themeStylesConst.normal);
    }
  }
  public saveEditForm(): void {
    if (this.editFormGroup.invalid) {
      this.editFormGroup.markAllAsTouched();
      return;
    }
      for (const data in this.editFormGroup.value) {
        if (this.editFormGroup.value[data]) {
          this.user[data] = this.editFormGroup.value[data];
        }
      }
      this.userService.update(this.user).pipe().subscribe(
        (data: UserModel) => {
          this.checkCurrentUser();
          this.location.back();
        },
        (err: ErrorModel<string, number>) => {
          this.errorMessage = 'Username already exist';
        }
      );
  }

  public backToPrevPage(): void {
    this.menuService.openMenuEvent();
    this.router.navigateByUrl('/tabs/library');
  }

  public check() {
    FingerprintAIO.isAvailable().then(result => {
      console.log(result);
    }).catch(err => {
      console.log(err);
    });
  }

  public show() {
    this.fingerprintService.setFingerPrint(this.registerFingerForm.value.username, this.registerFingerForm.value.password);
  }

  ionViewWillLeave() {
    this.editFormGroup.reset();
    this.registerFingerForm.reset();
  }



}
