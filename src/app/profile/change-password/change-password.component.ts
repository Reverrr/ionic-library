import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@app/shared/services';
import { UserModel } from '@app/shared/models';

@Component({
  selector: 'app-change-password',
  templateUrl: 'change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {

    public passwordError: string;
    public user: UserModel;

  public editPasswordForm: FormGroup = new FormGroup({
    currentPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]),
    newPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]),
  });

  constructor(
    public userService: UserService,
    ) {}

  public savePassword(): void {
    this.userService.checkUserPassword(this.user, this.editPasswordForm.value).subscribe(
      (data: {data: UserModel, status: number}) => {
        this.passwordError = '';
        if (data.status === 201) {
          this.user = data.data;
        }
      },
      (err: string) => {
        this.passwordError = err;
        throw err;
       }
    );
  }

  ionViewWillLeave() {
    this.editPasswordForm.reset();
  }
}
