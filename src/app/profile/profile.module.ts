// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Components
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile.component';
import { SharedModule } from '@app/shared/shared.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

const routes: Routes = [
    {
        path: '',
        component: ProfileComponent,
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        ChangePasswordComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        ProfileComponent,
        ChangePasswordComponent
    ],
    providers: [
        LocalNotifications,
    ]
})

export class ProfileModule { }
