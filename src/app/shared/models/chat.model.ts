export class ChatModel {
    online: number;
    statusWindow: boolean;
    message: string;
    username?: string;
    date?: Date;
    color?: boolean;
}
