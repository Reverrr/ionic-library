﻿export class UserModel {
    id: string;
    username: string;
    email: string = '';
    images: string[];
    password: string;
    token: string;
    role = 'user';
}
