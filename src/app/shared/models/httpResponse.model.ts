export interface HttpModel<T> {
  data: T;
  status: number;
  message?: string;
}
