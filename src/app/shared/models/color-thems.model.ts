export class ColorThemeModel {
    public main: string;
    public shade: string;
    public tint: string;
}
