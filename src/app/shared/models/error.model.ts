export class ErrorModel<D, E> {
  public description = 'Unhandled Error';
  public data: D;
  public error: E;
}
