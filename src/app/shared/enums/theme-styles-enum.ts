export const themeStylesEnum = {
    light: 'light',
    dark: 'dark',
    toxic: 'toxic',
    normal: 'normal'
  };
