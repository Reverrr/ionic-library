// Vendors
import { Component } from '@angular/core';
import { MenuService } from '../../services/menu.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})

export class MenuComponent {
    public toggled = false;
    public menuIsOpen: boolean;

    constructor(private menuService: MenuService) {
        this.menuService.getMenuStatus().subscribe((status: boolean) => {
            this.menuIsOpen = status;
        });
    }

    public toggle(): void {
        this.toggled = !this.toggled;
    }

    public cancelSearch(event: Event) {
        this.toggled = false;
    }

    public openFirst(): void {
        this.menuService.openMenuEvent();
    }
}
