import { Component } from '@angular/core';

@Component({
    selector: 'app-sliders',
    templateUrl: './sliders.component.html',
    styleUrls: ['./sliders.component.scss']
  })
export class SliderComponent {
    public slideOpts = {
        initialSlide: 0,
        speed: 300,
        loop: true,
        autoplay: {
            delay: 1000,
          },
      };
      constructor() {}
}
