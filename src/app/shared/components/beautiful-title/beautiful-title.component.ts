import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-beautiful-title',
  templateUrl: './beautiful-title.component.html',
  styleUrls: ['./beautiful-title.component.scss'],
})
export class BeautifulTitleComponent implements OnInit {

  constructor() { }

  @Input()
  public title;

  @Input()
  public idForPattern;

  ionViewWillEnter() {
  }

  ngOnInit() {

  }

  changeTitle() {

  }

}
