//Vendors
import { Component, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { AlertController } from '@ionic/angular';
//Services
import { LayoutService, LibraryService } from "@app/shared/services";
//Models
import { AuthorModel } from "@app/shared/models";

@Component({
    selector: 'app-drop-down-authors',
    templateUrl: './drop-down-authors.component.html',
    styleUrls: ['./drop-down-authors.component.scss']
})

export class DropDownAuthorsComponent {
    public selectedAuthors: string[] = [];
    public title: string;
    public authors: AuthorModel[] = [];
    private allAuthorsClone: AuthorModel[] = [];
    public dropMenuToggle: boolean;
    public newAuthor: AuthorModel = new AuthorModel();
    public notAllInputFilled: boolean;
    public ionAllAuthors: any[] = [];
    public layoutSubscription: Subscription;

    @Output() onChanged = new EventEmitter<string[]>();

    public addAuthorForm: FormGroup = new FormGroup({
        author: new FormControl('', []),
        selectAuthor: new FormControl('', []),
    });

    constructor(
        private layout: LayoutService,
        public libraryService: LibraryService,
        public alertController: AlertController
    ) {
        this.getAuthors();
    }

    public onSelect(event: Event): void {
        event.stopPropagation();
    }

    public async presentAlert() {
        const alert = await this.alertController.create({
            header: 'Select Authors',
            inputs: this.ionAllAuthors,
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {}
              }, {
                text: 'Ok',
                handler: (event: string[]) => {
                    this.ionAllAuthors.forEach((item) => item.checked = false);
                    event.forEach((element, index) => {
                       let indexAuthorSelect = this.ionAllAuthors.findIndex((i) => i.name === element);
                       this.ionAllAuthors[indexAuthorSelect].checked = true;
                    })
                    this.selectedAuthors = event;
                    this.onChanged.emit(this.selectedAuthors);
                }
              }
            ]
          });
      
          await alert.present();
      }

    // public selectedOption(index: number): void {
    //     this.selectedAuthors.push(this.authors[index].name);
    //     this.authors.splice(index, 1);
    //     this.onChanged.emit(this.selectedAuthors);
    // }
    
    // public removeAuthor(option: string, index: number): void {
    //     const indexInCloneAuthor = this.allAuthorsClone.findIndex(i => i.name === option);
    //     this.selectedAuthors.splice(index, 1);
    //     this.authors.push(this.allAuthorsClone[indexInCloneAuthor]);
    //     this.onChanged.emit(this.selectedAuthors);
    // }

    private getAuthors(): void {
        this.libraryService.getAllAuthors().subscribe(
            (allAuthors: AuthorModel[]) => {
                this.authors = allAuthors;
                this.allAuthorsClone = allAuthors.slice();
                allAuthors.forEach((item) => {
                    this.ionAllAuthors.push({
                        name: item.name,
                        type: 'checkbox',
                        label: item.name,
                        value: item.name,
                        checked: false
                    });

                })
            }
        );
    }
}