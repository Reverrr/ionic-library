import { Injectable } from '@angular/core';
import { AndroidFingerprintAuth, AFAEncryptResponse } from '@ionic-native/android-fingerprint-auth/ngx';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class FingerprintService {

  constructor(
    private androidFingerprintAuth: AndroidFingerprintAuth,
    public toastController: ToastController,
    ) {}

    async presentToast() {
      const toast = await this.toastController.create({
        message: 'Your finger have been saved.',
        duration: 2000
      });
      toast.present();
    }

  public setFingerPrint(username: string, password: string) {
    this.androidFingerprintAuth.isAvailable()
      .then((result) => {
        if (result.isAvailable) {
          this.androidFingerprintAuth.encrypt({
            clientId: 'currentUser',
            dialogTitle: 'Create finger signIn',
            disableBackup: true,
            password: `${JSON.stringify({
              username: username,
              password: password
            })}`
          }).then((encryptResult: AFAEncryptResponse) => {
              if (encryptResult.withFingerprint) {
                localStorage.setItem('tokenForFinger', encryptResult.token);
                this.presentToast();
              } else if (encryptResult.withBackup) {
                console.log('Successfully authenticated with backup password!');
              } else {
                console.log('Didn\'t authenticate!');
              }
            })
            .catch(error => {
              if (error === this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                console.log('Fingerprint authentication cancelled');
              } else {
                console.error(error);
              }
            });

        } else {
          // fingerprint auth isn't available
        }
      })
      .catch(error => console.error(error));
  }
}
