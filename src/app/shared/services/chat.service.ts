// Vendors
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { BehaviorSubject, Observable } from 'rxjs';
// Services
import { ChatModel } from '../models/chat.model';
import { AuthenticationService } from '@app/shared/services';
// Models

@Injectable({ providedIn: 'root' })
export class ChatService {
    private statusChat: ChatModel = {statusWindow: true, online: 0, message: ''};
    private statusChatBS: BehaviorSubject<ChatModel> = new BehaviorSubject<ChatModel>(this.statusChat);

    private messageArray: ChatModel[] = [];
    private messageArrayBS: BehaviorSubject<ChatModel[]> = new BehaviorSubject<ChatModel[]>(this.messageArray);

    constructor(
        private socket: Socket,
        private auth: AuthenticationService
        ) { }

    public getMessage(): Observable<ChatModel> {
        return this.socket.fromEvent('events');
    }

    public getTotalOnlineUsers(): Observable<number> {
        return this.socket.fromEvent('users');
    }

    public sendMessage(msg: string): void {
        const time = new Date().toTimeString().split(' ')[0];
        const currentUser = this.auth.getCurrentUser().subscribe((data) => {
            this.socket.emit('events', {username: (data && data.username) ? data.username : 'unknown', message: msg, time: time}, () => {
                currentUser.unsubscribe();
            });
        });
    }

    public setMessageArray(messages: ChatModel[]): void {
        this.messageArray = messages;
        this.messageArrayBS.next(messages);
    }

    public getMessageArray(): Observable<ChatModel[]> {
        return this.messageArrayBS.asObservable();
    }

    public getChatStatus(): Observable<ChatModel> {
        return this.statusChatBS.asObservable();
    }

    public setChatOptions(status: boolean, online: number): void {
        this.statusChat.statusWindow = status;
        this.statusChat.online = online;
        this.statusChatBS.next(this.statusChat);
    }
}
