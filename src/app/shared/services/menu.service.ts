//Vendors
import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';
//Models
import { BookModel, AuthorModel, CountModel } from '@app/shared/models';
//Environment
import { environment } from '@environments/environment';
//Services
import { PropertyNameChangeHelper } from '@app/shared/services/property-name-change.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class MenuService {
    private menuIsOpen: boolean;
    private menuIsOpenBS: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.menuIsOpen);

    constructor(private menu: MenuController) {}
    
    public openMenuEvent(): void {
        //   if (this.menuIsOpen) {
        //     this.menuIsOpen = false;
        //     this.menu.close('first');
        //   } else {        
            // this.menuIsOpen = true;
            this.menu.open('first');
            this.menuIsOpen = true;
            this.menuIsOpenBS.next(true);
        //   }
        }
        public closeMenuEvent() {
            this.menu.close('first');
            this.menuIsOpenBS.next(false);
            // this.menuIsOpen = false;
        }

        public getMenuStatus(): Observable<boolean> {
            return this.menuIsOpenBS.asObservable();
        }
}