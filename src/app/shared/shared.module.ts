// Vendors
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Components
import { DropDownAuthorsComponent } from '@app/shared/components/drop-down-authors/drop-down-authors.component';
import { NgxPaginationModule } from 'ngx-pagination';
// Pipes
// Modules
import { HeaderComponent } from '@app/shared/header/header.component';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from '@app/shared/components/menu-component/menu.component';
import { PopoverPage } from './components/popover/popover.component';
import { SliderComponent } from './components/sliders/sliders.component';
import { BeautifulTitleComponent } from './components/beautiful-title/beautiful-title.component';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { HttpModule } from '@angular/http';
import { File } from '@ionic-native/file/ngx';

@NgModule({
    declarations: [
        DropDownAuthorsComponent,
        HeaderComponent,
        MenuComponent,
        PopoverPage,
        SliderComponent,
        BeautifulTitleComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        HttpModule,
        NgxPaginationModule,
        ReactiveFormsModule,
    ],
    exports: [
        DropDownAuthorsComponent,
        HeaderComponent,
        MenuComponent,
        PopoverPage,
        SliderComponent,
        CommonModule,
        NgxPaginationModule,
        FormsModule,
        BeautifulTitleComponent
    ],
    providers: [
        AndroidFingerprintAuth,
        FileTransfer,
        File,
    ]
})

export class SharedModule {}
