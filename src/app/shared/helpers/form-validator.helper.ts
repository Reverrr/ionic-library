import { AbstractControl } from '@angular/forms';
import { KeyValueModel } from '../models/key-value.model';

export class FormValidatorHelper {
  public static email(input: AbstractControl): KeyValueModel<boolean> | null {
    if (!input || input.value === null) {
      return { email: true };
    }

    const re: RegExp = new RegExp(''
      + /^(([^<>()\[\]\\.,;:\s@'`"]+(\.[^<>()\[\]\\.,;:\s@'`"]+)*)|(".+"))@/.source
      + /((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.source
    );

    return (
      input.value.match(re)
        ? null
        : { email: true }
    );
  }

    public static username(input: AbstractControl): KeyValueModel<boolean | null> {
        if (!input || input.value === null) {
            return { username: true };
        }
        const re: RegExp = new RegExp(''
            + /^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g.source
        );

        return (
            input.value.match(re)
                ? null
                : { username: true }
        );
    }

    public static password(input: AbstractControl): KeyValueModel<boolean | null> {
        if (!input || input.value === null) {
            return { password: true };
        }
        const re: RegExp = new RegExp(''
            + /^(?=.*\d)(?=.*[A-z]).{8,16}$/.source
        );

        return (
            input.value.match(re)
                ? null
                : { password: true }
        );
    }
}
