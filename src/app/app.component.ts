import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserService } from './shared/services/user.service';
import { ColorThemeModel } from './shared/models/color-thems.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {

  constructor (
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userService: UserService,
    ) {
    this.userService.getColorTheme().subscribe((color: ColorThemeModel) => {
      document.documentElement.style.setProperty('--ion-color-primary', color.main);
      document.documentElement.style.setProperty('--ion-color-primary-shade', color.shade);
      document.documentElement.style.setProperty('--ion-color-primary-tint', color.tint);
    });

    this.initializeApp();
    this.platform.ready().then((data) => {
      if (this.platform.is('cordova')) {
        // make your native API calls
      } else {
        // fallback to browser APIs
      }

    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
