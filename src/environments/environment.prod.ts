export const environment = {
  production: true,
  apiUrl: 'https://library-nest.herokuapp.com',
  defaultBookImgUrl: 'https://countrylakesdental.com/wp-content/uploads/2016/10/orionthemes-placeholder-image.jpg',
};
