FROM node:8
WORKDIR /app
COPY . /app/
COPY package*.json ./
RUN rm -rf www && rm -rf node_modules && npm install -g ionic && npm install && npm rebuild node-sass --force && dir && rm -rf build && npm run build
EXPOSE 8100
CMD ["npm", "start"]